/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/JSP_Servlet/Servlet.java to edit this template
 */
package Controllers;

import DAL.AppointmentDAO;
import DAL.BillingHistoryDAO;
import DAL.SubLevelMenuDAO;
import Models.Appointments;
import Models.BillingHistory;
import Models.Employee;
import java.io.IOException;
import java.io.PrintWriter;
import jakarta.servlet.ServletException;
import jakarta.servlet.http.HttpServlet;
import jakarta.servlet.http.HttpServletRequest;
import jakarta.servlet.http.HttpServletResponse;
import jakarta.servlet.http.HttpSession;
import java.time.LocalDate;
import java.time.format.DateTimeFormatter;
import java.util.ArrayList;

/**
 *
 * @author phuon
 */
public class AdminPaymentHistoryController extends HttpServlet {

    private final String STATISTIC_APPOINTMENT = "admin-payment/admin-payment-history.jsp";
    private final String NEED_EMPLOYEE = "admin-screen/admin-login.jsp";
    private final String NEED_LOGIN = "Bạn cần đăng nhập truy cập vào trang này";
    private final String NEED_ROLE = "Bạn cần có quyền để truy cập vào trang này";
    private final String[] ROLE = {"1", "2", "5"};

    /**
     * Processes requests for both HTTP <code>GET</code> and <code>POST</code>
     * methods.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    protected void processRequest(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        response.setContentType("text/html;charset=UTF-8");
    }

    // <editor-fold defaultstate="collapsed" desc="HttpServlet methods. Click on the + sign on the left to edit the code.">
    /**
     * Handles the HTTP <code>GET</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        HttpSession session = request.getSession();
        Employee checkEmp = (Employee) session.getAttribute("EMPLOYEE");
        //check login
        if (checkEmp == null) {
            request.setAttribute("MESSAGE", NEED_LOGIN);
            request.getRequestDispatcher(NEED_EMPLOYEE).forward(request, response);
        } else {
            try {
                String empRole = checkEmp.getEmployeeRole().getId();
                //check the role of employee
                for (String roleNeed : ROLE) {
                    if (empRole.equals(roleNeed)) {
                        throw new Exception();
                    }
                }
                request.setAttribute("MESSAGE", NEED_ROLE);
                request.getRequestDispatcher(NEED_EMPLOYEE).forward(request, response);
            } catch (Exception e) {
            }
            try {
                BillingHistoryDAO bhList = new BillingHistoryDAO();
                int page = 1;
                final int recordsPerPage = 5;
                //set start page = 1
                if (request.getParameter("page") != null) {
                    page = Integer.parseInt(request.getParameter("page"));
                }
                ArrayList<BillingHistory> list = bhList.getListBillingHistory((page - 1) * recordsPerPage, recordsPerPage);
                int recordCount = bhList.countAllBillingHistory();
                int pageCount = (int) Math.ceil(recordCount * 1.0 / recordsPerPage);
                request.setAttribute("pageCount", pageCount);
                request.setAttribute("currentPage", page);
                request.setAttribute("bhList", list);
                request.getRequestDispatcher(STATISTIC_APPOINTMENT).forward(request, response);
            } catch (ServletException | IOException | NumberFormatException e) {
                System.out.println("AdminPaymentHistoryController.doGet: " + e);
            }
        }
    }

    /**
     * Handles the HTTP <code>POST</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        HttpSession session = request.getSession();
        Employee checkEmp = (Employee) session.getAttribute("EMPLOYEE");
        BillingHistoryDAO bhDAO = new BillingHistoryDAO();
        AppointmentDAO adao = new AppointmentDAO();

        int page = 1;
        final int recordsPerPage = 5;
        String total, id;

        //check login
        if (checkEmp == null) {
            request.setAttribute("MESSAGE", NEED_LOGIN);
            request.getRequestDispatcher(NEED_EMPLOYEE).forward(request, response);
        }
        String method = request.getParameter("method");
        switch (method) {
            case "search":
                String search = request.getParameter("search");
                BillingHistoryDAO bhList = new BillingHistoryDAO();
                //set start page = 1
                if (request.getParameter("page") != null) {
                    page = Integer.parseInt(request.getParameter("page"));
                }
                ArrayList<BillingHistory> list = bhList.getListBillingHistoryByPatientName((page - 1) * recordsPerPage, recordsPerPage, search);
                int recordCount = bhList.countAllBillingHistory();
                int pageCount = (int) Math.ceil(recordCount * 1.0 / recordsPerPage);
                request.setAttribute("pageCount", pageCount);
                request.setAttribute("currentPage", page);
                request.setAttribute("bhList", list);
                request.getRequestDispatcher(STATISTIC_APPOINTMENT).forward(request, response);
                break;
            default:
                break;
        }
    }

    /**
     * Returns a short description of the servlet.
     *
     * @return a String containing servlet description
     */
    @Override
    public String getServletInfo() {
        return "Short description";
    }// </editor-fold>

}
