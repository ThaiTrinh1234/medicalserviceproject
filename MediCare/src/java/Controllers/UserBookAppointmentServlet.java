/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/JSP_Servlet/Servlet.java to edit this template
 */
package Controllers;

import DAL.AdminEmailContext;
import DAL.AppointmentsDAO;
import DAL.BranchDAO;
import DAL.EmployeeDAO;
import DAL.FamilyProfileDAO;
import DAL.RelationshipDAO;
import DAL.ScheduleDetailDAO;
import DAL.ServiceTagDAO;
import DAL.UserDAO;
import Models.Branch;
import Models.FamilyProfile;
import Models.Relationship;
import Models.ServiceTag;
import Models.User;
import com.google.gson.JsonObject;
import com.itextpdf.html2pdf.ConverterProperties;
import com.itextpdf.html2pdf.HtmlConverter;
import com.itextpdf.io.font.FontProgram;
import com.itextpdf.io.font.FontProgramFactory;
import com.itextpdf.layout.font.FontProvider;
import jakarta.servlet.RequestDispatcher;
import java.io.IOException;
import java.io.PrintWriter;
import jakarta.servlet.ServletException;
import jakarta.servlet.ServletOutputStream;
import jakarta.servlet.http.HttpServlet;
import jakarta.servlet.http.HttpServletRequest;
import jakarta.servlet.http.HttpServletResponse;
import jakarta.servlet.http.HttpServletResponseWrapper;
import jakarta.servlet.http.HttpSession;
import java.io.ByteArrayInputStream;
import java.io.ByteArrayOutputStream;
import java.io.CharArrayWriter;
import java.io.StringWriter;
import java.sql.Date;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Random;

/**
 *
 * @author tubinh
 */
public class UserBookAppointmentServlet extends HttpServlet {

    /**
     * Processes requests for both HTTP <code>GET</code> and <code>POST</code>
     * methods.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    protected void processRequest(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        response.setContentType("text/html;charset=UTF-8");
        try ( PrintWriter out = response.getWriter()) {
            /* TODO output your page here. You may use following sample code. */
            out.println("<!DOCTYPE html>");
            out.println("<html>");
            out.println("<head>");
            out.println("<title>Servlet UserBookingAppointmentServlet</title>");
            out.println("</head>");
            out.println("<body>");
            out.println("<h1>Servlet UserBookingAppointmentServlet at " + request.getContextPath() + "</h1>");
            out.println("</body>");
            out.println("</html>");
        }
    }

    // <editor-fold defaultstate="collapsed" desc="HttpServlet methods. Click on the + sign on the left to edit the code.">
    /**
     * Handles the HTTP <code>GET</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        System.out.println("doGet - USER - BOOK - APPOINTMENT:");
        BranchDAO bd = new BranchDAO();
        ArrayList<Branch> branches = bd.getAllBranches();

        Calendar calendar = Calendar.getInstance();
        Date currentDate = new Date(calendar.getTimeInMillis());

        ServiceTagDAO std = new ServiceTagDAO();
        ArrayList<ServiceTag> services = std.getAllServiceTags();

        HttpSession session = request.getSession();
        String email = (String) session.getAttribute("email");
        UserDAO ud = new UserDAO();
        User u = ud.login(email);
        if (u != null) {
            System.out.println("user != null");
            FamilyProfileDAO fpd = new FamilyProfileDAO();
            ArrayList<FamilyProfile> profiles = (ArrayList<FamilyProfile>) fpd.getFamilyProfileListByUserOwnerIdForBooking(u.getId());
            System.out.println("Profiles:");
            for (FamilyProfile profile : profiles) {
                System.out.println(profile);
            }
            request.setAttribute("profiles", profiles);
            request.setAttribute("ownerId", u.getId());
        }
        RelationshipDAO rDAO = new RelationshipDAO();
        ArrayList<Relationship> rList = rDAO.getRelationshipList();
        request.setAttribute("rList", rList);
        request.setAttribute("branches", branches);
        request.setAttribute("services", services);
        request.setAttribute("currentDate", currentDate);

        request.getRequestDispatcher("user-book-appointment.jsp").forward(request, response);
    }

    /**
     * Handles the HTTP <code>POST</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        System.out.println("doPost - USER - BOOK - APPOINTMENT:");
        response.setContentType("text/html;charset=UTF-8");
        String action = request.getParameter("action");
        HttpSession session = request.getSession();
        String branchId, serviceId, doctorId, date, slotId;
        branchId = request.getParameter("branchId");
        serviceId = request.getParameter("serviceId");
        doctorId = request.getParameter("doctorId");
        date = request.getParameter("date");
        slotId = request.getParameter("slotId");
        String patientName = request.getParameter("patientName");
        String gender = request.getParameter("gender");
        String birthDate = request.getParameter("birthDate");
        String phone = request.getParameter("phone");
        String emailPatient = request.getParameter("email");
        String symptoms = request.getParameter("description");
        String otp = request.getParameter("otp");
        String trueOTP = request.getParameter("trueOTP");

//        String password = request.getParameter("password");
        String email = (String) session.getAttribute("email");
        String password = (String) session.getAttribute("password");
        UserDAO ud = new UserDAO();
        User u = ud.login(email);

        java.util.Date currentDate = new java.util.Date();
        java.sql.Timestamp createdAt = new java.sql.Timestamp(currentDate.getTime());

        AppointmentsDAO ad = new AppointmentsDAO();
//        ad.test(u==null?null:u.getId());

        ScheduleDetailDAO sdd = new ScheduleDetailDAO();
        System.out.println("SlotId: " + slotId);
        String startTime = sdd.getStartTimeBySlotId(slotId);

        System.out.println("Line 150: date + start-time = " + date + startTime);
        String plannedAt = date + " " + startTime;

        if (action != null) {
            switch (action) {
                case "requestOTP": {
                    System.out.println("Request OTP:");
                    // send Email:
                    email = emailPatient.trim();

                    String OTP = AdminEmailContext.generateRandomVerificationCode(6);

                    AdminEmailContext.sendVerificationCodeToEmail(email, OTP);
                    JsonObject jsonObject = new JsonObject();
                    jsonObject.addProperty("message", "Thành công! Hãy kiểm tra email để lấy mã OTP");
                    jsonObject.addProperty("OTP", OTP);

                    response.getWriter().write(OTP);
                    // end - send email:
                    break;
                }
                case "submitOTP": {
                    if (otp != null && otp.equals(trueOTP)) {
                        System.out.println("input OTP success");
                        if (ad.addNewAppointment(u == null ? null : u.getId(), branchId, serviceId, doctorId, plannedAt, slotId, createdAt.toString(),
                                patientName, gender, birthDate, phone, emailPatient, symptoms, email, password) && sdd.setStatusForBookingSlot(slotId, doctorId, date)) {
                            System.out.println("Add appointment to database successfully!");
                            System.out.println("Planned at :.... = " + plannedAt);
                        } else {
                            System.out.println("Planned at :.... = " + plannedAt);
                            System.out.println("Add appointment to database FAIL!!!");
                        }
                        System.out.println("email = " + email + " | branchId = " + branchId + " | serviceId = " + serviceId + " | doctorId = " + doctorId + " | date = " + date + " | slotId = " + slotId);
                        if (doctorId.equals("-1")) {
                            response.getWriter().write("Đặt lịch khám thành công! Lịch hẹn của bạn sẽ được xử lí và thông tin chi tiết sẽ được gửi đến email" + emailPatient + ".");
                        } else {
//                            // set 
//                            String infoSendMail = "";
//                            session.setAttribute("info-to-send-mail", u);
                            response.getWriter().write("Đặt lịch khám thành công! Thông tin lịch hẹn đã được gửi đến email " + emailPatient + ".");
                        }
                    } else {
                        System.out.println("input OTP fail");
                        response.getWriter().write("Mã OTP không khớp, vui lòng chọn 'Đặt hẹn' để nhận OTP mới!");
                    }
                    break;
                }
                case "send-pdf": {
                    System.out.println("CASE - send-pdf:");
                    email = emailPatient.trim();
                    try {
                        //Get HTML string from JSP file
                        RequestDispatcher dispatcher = getServletContext().getRequestDispatcher("/user-book-appointment.jsp");

                        StringWriter writer = new StringWriter();
                        PrintWriter out = new PrintWriter(writer);
                        dispatcher.include(request, new HttpServletResponseWrapper(response) {
                            @Override
                            public PrintWriter getWriter() {
                                return out;
                            }
                        });

                        //Create font
                        String fontPath1 = getServletContext().getRealPath("assets/fonts/Inter Desktop/Inter-Regular.otf");
                        String fontPath2 = getServletContext().getRealPath("assets/fonts/Inter Desktop/Inter-Light.otf");
                        String fontPath3 = getServletContext().getRealPath("assets/fonts/Inter Desktop/Inter-Medium.otf");
                        String fontPath4 = getServletContext().getRealPath("assets/fonts/Inter Desktop/Inter-Black.otf");
                        String fontPath5 = getServletContext().getRealPath("assets/fonts/Inter Desktop/Inter-Bold.otf");
                        String fontPath6 = getServletContext().getRealPath("assets/fonts/Inter Desktop/Inter-ExtraBold.otf");
                        String fontPath7 = getServletContext().getRealPath("assets/fonts/Inter Desktop/Inter-SemiBold.otf");
                        String fontPath8 = getServletContext().getRealPath("assets/fonts/Inter Desktop/Inter-Thin.otf");
                        String fontPath9 = getServletContext().getRealPath("assets/fonts/Inter Desktop/Inter-Italic.otf");

                        FontProgram fontProgram1 = FontProgramFactory.createFont(fontPath1);
                        FontProgram fontProgram2 = FontProgramFactory.createFont(fontPath2);
                        FontProgram fontProgram3 = FontProgramFactory.createFont(fontPath3);
                        FontProgram fontProgram4 = FontProgramFactory.createFont(fontPath4);
                        FontProgram fontProgram5 = FontProgramFactory.createFont(fontPath5);
                        FontProgram fontProgram6 = FontProgramFactory.createFont(fontPath6);
                        FontProgram fontProgram7 = FontProgramFactory.createFont(fontPath7);
                        FontProgram fontProgram8 = FontProgramFactory.createFont(fontPath8);
                        FontProgram fontProgram9 = FontProgramFactory.createFont(fontPath9);

                        //Create FontProvider and add the font
                        FontProvider fontProvider = new FontProvider();
                        fontProvider.addFont(fontProgram1);
                        fontProvider.addFont(fontProgram2);
                        fontProvider.addFont(fontProgram3);
                        fontProvider.addFont(fontProgram4);
                        fontProvider.addFont(fontProgram5);
                        fontProvider.addFont(fontProgram6);
                        fontProvider.addFont(fontProgram7);
                        fontProvider.addFont(fontProgram8);
                        fontProvider.addFont(fontProgram9);

                        //Create ConverterProperties and set the FontProvider
                        ConverterProperties converterProperties = new ConverterProperties();
                        converterProperties.setFontProvider(fontProvider);

                        String htmlString = writer.toString();
                        System.out.println(htmlString); // In ra mã HTML

                        //Convert HTML to PDF
                        ByteArrayInputStream htmlStream = new ByteArrayInputStream(htmlString.getBytes("UTF-8"));
                        ByteArrayOutputStream pdfStream = new ByteArrayOutputStream();
                        HtmlConverter.convertToPdf(htmlStream, pdfStream, converterProperties);

                        //Send PDF to client
                        byte[] pdfBytes = pdfStream.toByteArray();

//                        if (AdminEmailContext.sendEmailWithAttachment(email, pdfBytes)) {
//                            System.out.println("Send pdf success");
//                        } else {
//                            System.out.println("Send pdf fail");
//                        }
                        String htmlContent = request.getParameter("htmlContent");

                        // Chuyển HTML thành PDF sử dụng iText
//                        pdfStream = new ByteArrayOutputStream();
//                        converterProperties = new ConverterProperties();
                        HtmlConverter.convertToPdf(new ByteArrayInputStream(htmlContent.getBytes("UTF-8")), pdfStream, converterProperties);

                        // Gửi PDF qua email hoặc làm bất kỳ điều gì khác
                        pdfBytes = pdfStream.toByteArray();

                        if (AdminEmailContext.sendEmailWithAttachment(email, pdfBytes)) {
                            System.out.println("Send pdf success");
                        } else {
                            System.out.println("Send pdf fail");
                        }
//                        sendEmailWithEmbeddedPDF("recipient@example.com", pdfBytes);

//                        try ( ServletOutputStream sos = response.getOutputStream()) {
//                            sos.write(pdfBytes);
//                            sos.flush();
//                        }
                    } catch (ServletException | IOException e) {
                        System.out.println("DoctorProfileToPDFServlet: " + e.getMessage());
                    }
//                    String OTP = AdminEmailContext.generateRandomVerificationCode(6);

                    break;
                }
//                case "send-pdf": {
//                    System.out.println("CASE - send-pdf:");
//                    email = emailPatient.trim();
//                    // Tạo bản sao của HttpServletResponse
//                    CharArrayWriter charArrayWriter = new CharArrayWriter();
//                    HttpServletResponseWrapper responseWrapper = new HttpServletResponseWrapper(response) {
//                        @Override
//                        public PrintWriter getWriter() {
//                            return new PrintWriter(charArrayWriter);
//                        }
//                    };
//
//                    // Gọi đến JSP và chuyển responseWrapper để nó ghi dữ liệu vào charArrayWriter
//                    RequestDispatcher dispatcher = getServletContext().getRequestDispatcher("/user-book-appointment.jsp");
//                    dispatcher.include(request, responseWrapper);
//
//                    // Lấy nội dung từ charArrayWriter
//                    String htmlContent = charArrayWriter.toString();
//
//                    // Chuyển HTML thành PDF sử dụng iText
//                    ByteArrayOutputStream pdfStream = new ByteArrayOutputStream();
//                    ConverterProperties converterProperties = new ConverterProperties();
//                    HtmlConverter.convertToPdf(new ByteArrayInputStream(htmlContent.getBytes()), pdfStream, converterProperties);
//
//                    // Gửi PDF qua email
//                    byte[] pdfBytes = pdfStream.toByteArray();
//                    AdminEmailContext.sendEmailWithAttachment(email, pdfBytes);
//                    break;
//                }

                default:
                    throw new AssertionError();
            }
        }
    }

    /**
     * Returns a short description of the servlet.
     *
     * @return a String containing servlet description
     */
    @Override
    public String getServletInfo() {
        return "Short description";
    }// </editor-fold>

}
