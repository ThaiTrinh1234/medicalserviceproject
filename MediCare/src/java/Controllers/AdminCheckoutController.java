/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/JSP_Servlet/Servlet.java to edit this template
 */
package Controllers;

import DAL.AppointmentDAO;
import DAL.BillingHistoryDAO;
import DAL.SubLevelMenuDAO;
import Models.Appointments;
import Models.BillingHistory;
import Models.Employee;
import java.io.IOException;
import java.io.PrintWriter;
import jakarta.servlet.ServletException;
import jakarta.servlet.http.HttpServlet;
import jakarta.servlet.http.HttpServletRequest;
import jakarta.servlet.http.HttpServletResponse;
import jakarta.servlet.http.HttpSession;
import java.time.LocalDate;
import java.time.format.DateTimeFormatter;
import java.util.ArrayList;

/**
 *
 * @author phuon
 */
public class AdminCheckoutController extends HttpServlet {

    /**
     * Processes requests for both HTTP <code>GET</code> and <code>POST</code>
     * methods.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    private final String STATISTIC_APPOINTMENT = "admin-payment/admin-checkout.jsp";
    private final String NEED_EMPLOYEE = "admin-screen/admin-login.jsp";
    private final String NEED_LOGIN = "Bạn cần đăng nhập truy cập vào trang này";
    private final String NEED_ROLE = "Bạn cần có quyền để truy cập vào trang này";
    private final String[] ROLE = {"1", "2", "5"};

    /**
     * Processes requests for both HTTP <code>GET</code> and <code>POST</code>
     * methods.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    protected void processRequest(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        response.setContentType("text/html;charset=UTF-8");
    }

    // <editor-fold defaultstate="collapsed" desc="HttpServlet methods. Click on the + sign on the left to edit the code.">
    /**
     * Handles the HTTP <code>GET</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        HttpSession session = request.getSession();
        Employee checkEmp = (Employee) session.getAttribute("EMPLOYEE");
        //check login
        if (checkEmp == null) {
            request.setAttribute("MESSAGE", NEED_LOGIN);
            request.getRequestDispatcher(NEED_EMPLOYEE).forward(request, response);
        } else {
            try {
                String empRole = checkEmp.getEmployeeRole().getId();
                //check the role of employee
                for (String roleNeed : ROLE) {
                    if (empRole.equals(roleNeed)) {
                        throw new Exception();
                    }
                }
                request.setAttribute("MESSAGE", NEED_ROLE);
                request.getRequestDispatcher(NEED_EMPLOYEE).forward(request, response);
            } catch (Exception e) {
            }
            try {
                //check the url is search
                if (request.getParameter("delete-appointment") != null || request.getParameter("search-appointment") != null) {
                    throw new AdminException.RedirecUrlException();
                }
                //get list appointment
                AppointmentDAO adao = new AppointmentDAO();
                SubLevelMenuDAO slmDao = new SubLevelMenuDAO();

                int page = 1;
                final int recordsPerPage = 5;
                //set start page = 1
                if (request.getParameter("page") != null) {
                    page = Integer.parseInt(request.getParameter("page"));
                }
                ArrayList<Appointments> list = adao.getListCompletedAppointment((page - 1) * recordsPerPage, recordsPerPage);
                //insert <th></th>
                ArrayList<String> titleList = slmDao.getTitleTable("titleTableAppointments");
                for (int i = 0; i < titleList.size(); i++) {
                    titleList.set(i, "<th>" + titleList.get(i) + "</th>");
                }
                int recordCount = adao.countCompletedAppointment();
                int pageCount = (int) Math.ceil(recordCount * 1.0 / recordsPerPage);
                request.setAttribute("pageCount", pageCount);
                request.setAttribute("currentPage", page);
                request.setAttribute("TITLE_APPOINTMENTS", titleList);
                session.setAttribute("ALL_APPOINTMENT", list);
                request.getRequestDispatcher(STATISTIC_APPOINTMENT).forward(request, response);
            } catch (AdminException.RedirecUrlException e) {
                try {
                    if (request.getParameter("search-appointment") != null) {
                        throw new AdminException.RedirecUrlException();
                    }
                } catch (AdminException.RedirecUrlException e2) {
                    AppointmentDAO dao = new AppointmentDAO();
                    String id = request.getParameter("id");
                    dao.deleteAppointmentById(id);
                    //get list appointment
                    SubLevelMenuDAO slmDao = new SubLevelMenuDAO();
                    ArrayList<Appointments> list = dao.getCompletedAppointment();
                    ArrayList<String> titleList = slmDao.getTitleTable("titleTableAppointments");
                    //insert <th></th>
                    for (int i = 0; i < titleList.size(); i++) {
                        titleList.set(i, "<th>" + titleList.get(i) + "</th>");
                    }
                    request.setAttribute("TITLE_APPOINTMENTS", titleList);
                    session.setAttribute("ALL_APPOINTMENT", list);
                    request.getRequestDispatcher(STATISTIC_APPOINTMENT).forward(request, response);
                }
            }
        }
    }

    /**
     * Handles the HTTP <code>POST</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        HttpSession session = request.getSession();
        Employee checkEmp = (Employee) session.getAttribute("EMPLOYEE");
        BillingHistoryDAO bhDAO = new BillingHistoryDAO();
        AppointmentDAO adao = new AppointmentDAO();

        int page = 1;
        final int recordsPerPage = 5;
        String total, id;

        //check login
        if (checkEmp == null) {
            request.setAttribute("MESSAGE", NEED_LOGIN);
            request.getRequestDispatcher(NEED_EMPLOYEE).forward(request, response);
        }
        String method = request.getParameter("method");
        switch (method) {
            case "checkout":
                id = request.getParameter("id");
                total = request.getParameter("amount");
                LocalDate date = LocalDate.now();
                DateTimeFormatter formatter = DateTimeFormatter.ofPattern("yyyy-MM-dd");
                String currentDate = date.format(formatter);
                BillingHistory bh = new BillingHistory();
                bh.setAppointmentId(id);
                bh.setCreatedAt(currentDate);
                bh.setTotalCash(total);
                bhDAO.addBill(bh);
                adao.setAppointmentStatus(4, id);
                response.sendRedirect("admin-payment");
                break;
            case "search":
                SubLevelMenuDAO slmDao = new SubLevelMenuDAO();
                ArrayList<String> titleList = slmDao.getTitleTable("titleTableAppointments");
                for (int i = 0; i < titleList.size(); i++) {
                    titleList.set(i, "<th>" + titleList.get(i) + "</th>");
                }
                String search = request.getParameter("search");
                ArrayList<Appointments> list = adao.getListCompletedAppointmentByPatientName((page - 1) * recordsPerPage, recordsPerPage, search);
                request.setAttribute("TITLE_APPOINTMENTS", titleList);
                session.setAttribute("ALL_APPOINTMENT", list);
                request.getRequestDispatcher(STATISTIC_APPOINTMENT).forward(request, response);
                break;
            default:
                break;
        }
    }

    /**
     * Returns a short description of the servlet.
     *
     * @return a String containing servlet description
     */
    @Override
    public String getServletInfo() {
        return "Short description";
    }// </editor-fold>

}
