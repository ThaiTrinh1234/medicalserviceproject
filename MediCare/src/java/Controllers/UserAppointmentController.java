/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/JSP_Servlet/Servlet.java to edit this template
 */
package Controllers;

import DAL.AppointmentsDAO;
import DAL.UserDAO;
import Models.Appointments;
import java.io.IOException;
import jakarta.servlet.ServletException;
import jakarta.servlet.http.HttpServlet;
import jakarta.servlet.http.HttpServletRequest;
import jakarta.servlet.http.HttpServletResponse;
import jakarta.servlet.http.HttpSession;
import java.text.ParseException;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 *
 * @author phuon
 */
public class UserAppointmentController extends HttpServlet {

    /**
     * Processes requests for both HTTP <code>GET</code> and <code>POST</code>
     * methods.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    protected void processRequest(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {

    }

    // <editor-fold defaultstate="collapsed" desc="HttpServlet methods. Click on the + sign on the left to edit the code.">
    /**
     * Handles the HTTP <code>GET</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        try {
            HttpSession session = request.getSession();
            
            UserDAO uDAO = new UserDAO();
            String ownerId = uDAO.getIdByEmail(String.valueOf(session.getAttribute("email")));
            
            AppointmentsDAO aDAO = new AppointmentsDAO();
            List<Appointments> aList = aDAO.getListAppointmentsByOwnerId(ownerId);
            
            if (session.getAttribute("email") == null) {
                response.sendRedirect("user-login");
            } else {
                request.setAttribute("aList", aList);
                request.getRequestDispatcher("user-appointment.jsp").forward(request, response);
            }
        } catch (ParseException ex) {
            Logger.getLogger(UserAppointmentController.class.getName()).log(Level.SEVERE, null, ex);
        }
    }

    /**
     * Handles the HTTP <code>POST</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        AppointmentsDAO aDAO = new AppointmentsDAO();
        HttpSession session = request.getSession();
        List<Appointments> aList;
        UserDAO uDAO = new UserDAO();

        String search;
        search = request.getParameter("search-appointment");
        
        String ownerId = uDAO.getIdByEmail(String.valueOf(session.getAttribute("email")));
        String id = request.getParameter("appointmentId");


        if (session.getAttribute("email") == null){
            response.sendRedirect("user-login");
        }
        else{
            try {
                String method = request.getParameter("method");
                switch(method){
                    case "search":
                        aList = aDAO.getListAppointmentsByPatientName(search,ownerId);
                        if(!aList.isEmpty()){
                            request.setAttribute("aList", aList);
                            request.getRequestDispatcher("user-appointment.jsp").forward(request, response);
                        }
                        else {
                            aList = aDAO.getListAppointmentsByPhone(search,ownerId);
                            
                            request.setAttribute("aList", aList);
                            request.getRequestDispatcher("user-appointment.jsp").forward(request, response);
                            
                        }
                        break;
                    case "cancel":
                        if(aDAO.cancelAppointmentById(id)){
                            aList = aDAO.getListAppointmentsByOwnerId(ownerId);
                            request.setAttribute("aList", aList);
                            request.getRequestDispatcher("user-appointment.jsp").forward(request, response);
                        }
                        break;
                    default:
                        throw new AssertionError();
                }
            } catch (ParseException ex) {
                Logger.getLogger(UserAppointmentController.class.getName()).log(Level.SEVERE, null, ex);
            }
        }
    }

    /**
     * Returns a short description of the servlet.
     *
     * @return a String containing servlet description
     */
    @Override
    public String getServletInfo() {
        return "Short description";
    }// </editor-fold>

}
