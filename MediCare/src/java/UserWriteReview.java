/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/JSP_Servlet/Servlet.java to edit this template
 */


import DAL.AppointmentsDAO;
import Models.Appointments;
import java.io.IOException;
import jakarta.servlet.ServletException;
import jakarta.servlet.http.HttpServlet;
import jakarta.servlet.http.HttpServletRequest;
import jakarta.servlet.http.HttpServletResponse;
import jakarta.servlet.http.HttpSession;

/**
 *
 * @author hoang
 */
public class UserWriteReview extends HttpServlet {
   
    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response)
    throws ServletException, IOException {
        HttpSession session = request.getSession();
        
        if (session.getAttribute("loginValue") == null) {
            session.setAttribute("error", "Bạn cần đăng nhập để truy cập trang này.");
            response.sendRedirect("user-login");
        } else {
            String appointmentId = request.getParameter("appointmentId");
            AppointmentsDAO ad = new AppointmentsDAO();
            Appointments a = ad.getCompletedAppointmentById(appointmentId);
            String rating = request.getParameter("rating");
            String content = request.getParameter("content");
            
            request.setAttribute("a", a);
            request.setAttribute("rating", rating);
            request.setAttribute("content", content);
            
            request.getRequestDispatcher("user-review.jsp").forward(request, response);
        }
    } 

    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response)
    throws ServletException, IOException {
        
    }
}
