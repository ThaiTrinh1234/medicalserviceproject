package DAL;

import Models.Appointments;
import Models.Doctor;
import java.util.Base64;
import java.util.Date;
import java.util.Properties;
import java.util.Random;
import javax.mail.Authenticator;
import javax.mail.Message;
import javax.mail.MessagingException;
import javax.mail.Multipart;
import javax.mail.PasswordAuthentication;
import javax.mail.Session;
import javax.mail.Transport;
import javax.mail.internet.InternetAddress;
import javax.mail.internet.MimeBodyPart;
import javax.mail.internet.MimeMessage;
import javax.mail.internet.MimeMultipart;

/**
 *
 * @author tranh
 */
public class AdminEmailContext {

    static String from = "thunvhe176252@fpt.edu.vn";
    static String password = "egej dvml estl papk";

    public static void sendEmailForgotPassword(String mail, String newPassword, String name) {
        String to = mail;
        String subject = "[MediCare]";
        String mailContent = "Xin chào " + name + "<br>"
                + "Hệ thống của chúng tôi vừa tạo mật khẩu ngẫu nhiên mới cho bạn: " + newPassword + "<br>"
                + "Hãy đăng nhập vào hệ thống bằng mật khẩu mới này";

        Properties props = new Properties();
        props.put("mail.smtp.host", "smtp.gmail.com");
        props.put("mail.smtp.socketFactory.port", "465");
        props.put("mail.smtp.socketFactory.class", "javax.net.ssl.SSLSocketFactory");
        props.put("mail.smtp.auth", "true");
        props.put("mail.smtp.prot", "465");

        // Create Authenticator
        Authenticator auth = new Authenticator() {
            @Override
            protected PasswordAuthentication getPasswordAuthentication() {
                return new PasswordAuthentication(from, password);
            }
        };

        Session session = Session.getInstance(props, auth);
        MimeMessage msg = new MimeMessage(session);
        try {
            msg.addHeader("Content-type", "text/html;charset=UTF-8");
            msg.setFrom(new InternetAddress(from));
            msg.setRecipients(Message.RecipientType.TO, InternetAddress.parse(to, false));
            msg.setSubject(subject);
            msg.setSentDate(new Date());
            msg.setContent(mailContent, "text/html;charset=UTF-8");
            Transport.send(msg);
        } catch (MessagingException e) {
        }
    }

    public static void sendEmailnewPassword(String mail, String newPassword, String name) {
        String to = mail;
        String subject = "Chào " + name;

        String mailContent = "Chúng tôi vừa tạo tài khoản mới cho bạn.<br>";
        mailContent += "Hãy đăng nhập vào hệ thống và đổi mật khẩu của bạn bằng tài khoản sau:<br>";
        mailContent += "Tài khoản: " + mail + "<br>";
        mailContent += "Mật khẩu: " + newPassword + "<br>";

        Properties props = new Properties();
        props.put("mail.smtp.host", "smtp.gmail.com");
        props.put("mail.smtp.socketFactory.port", "465");
        props.put("mail.smtp.socketFactory.class", "javax.net.ssl.SSLSocketFactory");
        props.put("mail.smtp.auth", "true");
        props.put("mail.smtp.prot", "465");

        // Create Authenticator
        Authenticator auth = new Authenticator() {
            @Override
            protected PasswordAuthentication getPasswordAuthentication() {
                return new PasswordAuthentication(from, password);
            }
        };

        Session session = Session.getInstance(props, auth);
        MimeMessage msg = new MimeMessage(session);
        try {
            msg.addHeader("Content-type", "text/HTML; charset = UTF-8");
            msg.setFrom(new InternetAddress(from));
            msg.setRecipients(Message.RecipientType.TO, InternetAddress.parse(to, false));
            msg.setSubject(subject);
            msg.setSentDate(new Date());
            msg.setContent(mailContent, "text/html;charset=UTF-8");
            Transport.send(msg);
        } catch (MessagingException e) {
        }
    }
    private static final String CHARACTERS = "ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz0123456789";
    private static final String NUMBERS = "0123456789";

    public String generatePassword(int length) {
        Random random = new Random();
        String newPassword = "" + CHARACTERS.charAt(random.nextInt(CHARACTERS.length()))
                + CHARACTERS.charAt(random.nextInt(CHARACTERS.length()))
                + CHARACTERS.charAt(random.nextInt(CHARACTERS.length()))
                + CHARACTERS.charAt(random.nextInt(CHARACTERS.length()))
                + CHARACTERS.charAt(random.nextInt(CHARACTERS.length()))
                + CHARACTERS.charAt(random.nextInt(CHARACTERS.length()));
        return newPassword;
    }

    public static String generateRandomVerificationCode(int length) {
        Random random = new Random();
        StringBuilder sb = new StringBuilder(length);
        for (int i = 0; i < length; i++) {
            int randomIndex = random.nextInt(NUMBERS.length());
            char randomChar = NUMBERS.charAt(randomIndex);
            sb.append(randomChar);
        }
        return sb.toString();
    }

    public static boolean sendVerificationCodeToEmail(String email, String code) {
        String subject = "[MediCare] Verification code";

        Properties props = new Properties();
        props.put("mail.smtp.host", "smtp.gmail.com");
        props.put("mail.smtp.socketFactory.port", "465");
        props.put("mail.smtp.socketFactory.class", "javax.net.ssl.SSLSocketFactory");
        props.put("mail.smtp.auth", "true");
        props.put("mail.smtp.prot", "465");

        // Create Authenticator
        Authenticator auth = new Authenticator() {
            @Override
            protected PasswordAuthentication getPasswordAuthentication() {
                return new PasswordAuthentication(from, password);
            }
        };

        Session session = Session.getInstance(props, auth);
        MimeMessage msg = new MimeMessage(session);
        try {
            msg.addHeader("Content-type", "text/HTML; charset = UTF-8");
            msg.setFrom(new InternetAddress(from));
            msg.setRecipients(Message.RecipientType.TO, InternetAddress.parse(email, false));
            msg.setSubject(subject);
            msg.setSentDate(new Date());
            msg.setContent("<table style=\"font-family:'Open Sans',sans-serif\" width=\"100%\" border=\"0\">\n"
                    + "        <tbody>\n"
                    + "            <tr>\n"
                    + "                <td style=\"word-break:break-word;padding:28px 33px 25px;font-family:'Open Sans',sans-serif\"\n"
                    + "                    align=\"left\">\n"
                    + "                    <div>\n"
                    + "                        <p style=\"font-size:14px;line-height:200%;\">\n"
                    + "                            Hi <strong>" + email + "</strong>, here is your verification code: </p>\n"
                    + "                        <p style=\"font-size:14px;line-height:200%;\">\n"
                    + "                            <strong>" + code + "</strong></p>\n"
                    + "                        <p style=\"font-size:14px;line-height:200%;\">\n"
                    + "                            Any questions please contact: <a href=\"mailto:medicare@gmail.com\"\n"
                    + "                                target=\"_blank\">medicare@gmail.com</a> to\n"
                    + "                            be answered.</p>\n"
                    + "                    </div>\n"
                    + "                </td>\n"
                    + "            </tr>\n"
                    + "        </tbody>\n"
                    + "    </table>", "text/html;charset=UTF-8");
            Transport.send(msg);
            return true;
        } catch (MessagingException e) {
        }
        return false;
    }

    public static boolean sendEmailWithAttachment(String email, byte[] attachment) {
        String subject = "[MediCare] - IMPORTANT!";

        Properties props = new Properties();
        props.put("mail.smtp.host", "smtp.gmail.com");
        props.put("mail.smtp.socketFactory.port", "465");
        props.put("mail.smtp.socketFactory.class", "javax.net.ssl.SSLSocketFactory");
        props.put("mail.smtp.auth", "true");
        props.put("mail.smtp.prot", "465");

        // Create Authenticator
        Authenticator auth = new Authenticator() {
            @Override
            protected PasswordAuthentication getPasswordAuthentication() {
                return new PasswordAuthentication(from, password);
            }
        };

        Session session = Session.getInstance(props, auth);
        MimeMessage msg = new MimeMessage(session);
        try {
            msg.addHeader("Content-type", "text/HTML; charset = UTF-8");
            msg.setFrom(new InternetAddress(from));
            msg.setRecipients(Message.RecipientType.TO, InternetAddress.parse(email, false));
            msg.setSubject(subject);
            msg.setSentDate(new Date());

// Attach the PDF
            MimeBodyPart pdfBodyPart = new MimeBodyPart();
            pdfBodyPart.setContent(attachment, "application/pdf");
            pdfBodyPart.setFileName("Lichkham.pdf");

            // Text content
            MimeBodyPart textBodyPart = new MimeBodyPart();
            textBodyPart.setText("Hello " + email + ", please see your details appointment's information in the following file.");

            Multipart multipart = new MimeMultipart();
            multipart.addBodyPart(textBodyPart);
            multipart.addBodyPart(pdfBodyPart);
            msg.setContent(multipart);
            Transport.send(msg);
            return true;
        } catch (MessagingException e) {
        }
        return false;
    }

    public static boolean sendEmailWithEmbeddedPDF(String email, byte[] pdfContent) {
        String subject = "[MediCare] Verification code";

        Properties props = new Properties();
        props.put("mail.smtp.host", "smtp.gmail.com");
        props.put("mail.smtp.socketFactory.port", "465");
        props.put("mail.smtp.socketFactory.class", "javax.net.ssl.SSLSocketFactory");
        props.put("mail.smtp.auth", "true");
        props.put("mail.smtp.prot", "465");

        // Create Authenticator
        Authenticator auth = new Authenticator() {
            @Override
            protected PasswordAuthentication getPasswordAuthentication() {
                return new PasswordAuthentication(from, password);
            }
        };

        Session session = Session.getInstance(props, auth);
        MimeMessage msg = new MimeMessage(session);
        try {
            msg.addHeader("Content-type", "text/html; charset=UTF-8");
            msg.setFrom(new InternetAddress(from));
            msg.setRecipients(Message.RecipientType.TO, InternetAddress.parse(email, false));
            msg.setSubject(subject);
            msg.setSentDate(new Date());

            // Create the HTML content with embedded PDF
            String htmlContent = "<html><body><p>Verification code:</p><object width=\"100%\" height=\"600\" data=\"data:application/pdf;base64,"
                    + Base64.getEncoder().encodeToString(pdfContent) + "\" type=\"application/pdf\"></object></body></html>";

            // Set the HTML content
            msg.setContent(htmlContent, "text/html");

            // Send the email
            Transport.send(msg);
            return true;
        } catch (MessagingException e) {
            e.printStackTrace(); // Handle the exception appropriately
        }
        return false;
    }

    public static boolean sendNewPassword(String email, String code) {
        String subject = "[MediCare] Quên mật khẩu";

        Properties props = new Properties();
        props.put("mail.smtp.host", "smtp.gmail.com");
        props.put("mail.smtp.socketFactory.port", "465");
        props.put("mail.smtp.socketFactory.class", "javax.net.ssl.SSLSocketFactory");
        props.put("mail.smtp.auth", "true");
        props.put("mail.smtp.prot", "465");

        // Create Authenticator
        Authenticator auth = new Authenticator() {
            @Override
            protected PasswordAuthentication getPasswordAuthentication() {
                return new PasswordAuthentication(from, password);
            }
        };

        Session session = Session.getInstance(props, auth);
        MimeMessage msg = new MimeMessage(session);
        try {
            msg.addHeader("Content-type", "text/HTML; charset = UTF-8");
            msg.setFrom(new InternetAddress(from));
            msg.setRecipients(Message.RecipientType.TO, InternetAddress.parse(email, false));
            msg.setSubject(subject);
            msg.setSentDate(new Date());
            msg.setContent("<table style=\"font-family:'Open Sans',sans-serif\" width=\"100%\" border=\"0\">\n"
                    + "        <tbody>\n"
                    + "            <tr>\n"
                    + "                <td style=\"word-break:break-word;padding:28px 33px 25px;font-family:'Open Sans',sans-serif\"\n"
                    + "                    align=\"left\">\n"
                    + "                    <div>\n"
                    + "                        <p style=\"font-size:14px;line-height:200%;\">\n"
                    + "                            Hi <strong>" + email + "</strong>, đây là mật khẩu mới của bạn: </p>\n"
                    + "                        <p style=\"font-size:14px;line-height:200%;\">\n"
                    + "                            <strong>" + code + "</strong></p>\n"
                    + "                        <p style=\"font-size:14px;line-height:200%;\">\n"
                    + "                            Nếu có bất cứ thắc mắc nào, vui lòng liên hệ: <a href=\"mailto:medicare@gmail.com\"\n"
                    + "                                target=\"_blank\">medicare@gmail.com</a> \n"
                    + "                            để được trả lời.</p>\n"
                    + "                    </div>\n"
                    + "                </td>\n"
                    + "            </tr>\n"
                    + "        </tbody>\n"
                    + "    </table>", "text/html;charset=UTF-8");
            Transport.send(msg);
            return true;
        } catch (MessagingException e) {
        }
        return false;
    }

    public static boolean sendAppointmentDetailsToEmailCancel(String email, Appointments appointment) {
        String subject = "[MediCare] Appointment Cancel";

        Properties props = new Properties();
        props.put("mail.smtp.host", "smtp.gmail.com");
        props.put("mail.smtp.socketFactory.port", "465");
        props.put("mail.smtp.socketFactory.class", "javax.net.ssl.SSLSocketFactory");
        props.put("mail.smtp.auth", "true");
        props.put("mail.smtp.prot", "465");

        // Create Authenticator
        Authenticator auth = new Authenticator() {
            @Override
            protected PasswordAuthentication getPasswordAuthentication() {
                return new PasswordAuthentication(from, password);
            }
        };

        Session session = Session.getInstance(props, auth);
        MimeMessage msg = new MimeMessage(session);
        try {
            msg.addHeader("Content-type", "text/HTML; charset = UTF-8");
            msg.setFrom(new InternetAddress(from));
            msg.setRecipients(Message.RecipientType.TO, InternetAddress.parse(email, false));
            msg.setSubject(subject);
            msg.setSentDate(new Date());
            msg.setContent(generateAppointmentDetailsHTMLCancel(email, appointment), "text/html;charset=UTF-8");
            Transport.send(msg);
            return true;
        } catch (MessagingException e) {
            e.printStackTrace(); // Handle or log the exception appropriately
        }
        return false;
    }
    public static boolean sendAppointmentDetailsToEmailAccept(String email, Appointments appointment) {
        String subject = "[MediCare] Appointment Accept";

        Properties props = new Properties();
        props.put("mail.smtp.host", "smtp.gmail.com");
        props.put("mail.smtp.socketFactory.port", "465");
        props.put("mail.smtp.socketFactory.class", "javax.net.ssl.SSLSocketFactory");
        props.put("mail.smtp.auth", "true");
        props.put("mail.smtp.prot", "465");

        // Create Authenticator
        Authenticator auth = new Authenticator() {
            @Override
            protected PasswordAuthentication getPasswordAuthentication() {
                return new PasswordAuthentication(from, password);
            }
        };

        Session session = Session.getInstance(props, auth);
        MimeMessage msg = new MimeMessage(session);
        try {
            msg.addHeader("Content-type", "text/HTML; charset = UTF-8");
            msg.setFrom(new InternetAddress(from));
            msg.setRecipients(Message.RecipientType.TO, InternetAddress.parse(email, false));
            msg.setSubject(subject);
            msg.setSentDate(new Date());
            msg.setContent(generateAppointmentDetailsHTMLAccept(email, appointment), "text/html;charset=UTF-8");
            Transport.send(msg);
            return true;
        } catch (MessagingException e) {
            e.printStackTrace(); // Handle or log the exception appropriately
        }
        return false;
    }

    private static String generateAppointmentDetailsHTMLCancel(String email, Appointments appointment) {
        StringBuilder htmlBuilder = new StringBuilder();
        htmlBuilder.append("<table style=\"font-family:'Open Sans',sans-serif\" width=\"100%\" border=\"0\">\n");
        htmlBuilder.append("    <tbody>\n");
        htmlBuilder.append("        <tr>\n");
        htmlBuilder.append("            <td style=\"word-break:break-word;padding:28px 33px 25px;font-family:'Open Sans',sans-serif\"\n");
        htmlBuilder.append("                align=\"left\">\n");
        htmlBuilder.append("                <div>\n");
        htmlBuilder.append("                    <p style=\"font-size:14px;line-height:200%;\">\n");
        htmlBuilder.append("                        Xin chào <strong>").append(email).append("</strong>, chúng tôi rất tiếc khi phải thông báo lịch khám của bạn đã bị hủy. </p>\n");
        htmlBuilder.append("                    <p style=\"font-size:14px;line-height:200%;\">\n");
//        htmlBuilder.append("                        <strong>Doctor:</strong> ").append(appointment.getDoctor()).append("</p>\n");
        // Add more appointment details as needed
        htmlBuilder.append("                </div>\n");
        htmlBuilder.append("            </td>\n");
        htmlBuilder.append("        </tr>\n");
        htmlBuilder.append("    </tbody>\n");
        htmlBuilder.append("</table>");
        return htmlBuilder.toString();
    }

    private static String generateAppointmentDetailsHTMLAccept(String email, Appointments appointment) {
        StringBuilder htmlBuilder = new StringBuilder();
        Doctor d = new DoctorDAO().getDoctorByDoctorId(appointment.getDoctor().getId());
        htmlBuilder.append("<table style=\"font-family:'Open Sans',sans-serif\" width=\"100%\" border=\"0\">\n");
        htmlBuilder.append("    <tbody>\n");
        htmlBuilder.append("        <tr>\n");
        htmlBuilder.append("            <td style=\"word-break:break-word;padding:28px 33px 25px;font-family:'Open Sans',sans-serif\"\n");
        htmlBuilder.append("                align=\"left\">\n");
        htmlBuilder.append("                <div>\n");
        htmlBuilder.append("                    <p style=\"font-size:14px;line-height:200%;\">\n");
        htmlBuilder.append("                        Xin chào <strong>").append(email).append("</strong>, yêu cầu khám bệnh của bạn đã được chấp nhận, sau đây là thông tin chi tiết: </p>\n");
        htmlBuilder.append("                    <p style=\"font-size:14px;line-height:200%;\">\n");
        htmlBuilder.append("                        <strong>Bác sĩ:</strong> ").append(d.getDisplayName()).append("</p>\n");
        htmlBuilder.append("                    <p style=\"font-size:14px;line-height:200%;\">\n");
        htmlBuilder.append("                        <strong>Vào lúc:</strong> ").append(appointment.getPlannedAt()).append("</p>\n");
        // Add more appointment details as needed
        htmlBuilder.append("                </div>\n");
        htmlBuilder.append("            </td>\n");
        htmlBuilder.append("        </tr>\n");
        htmlBuilder.append("    </tbody>\n");
        htmlBuilder.append("</table>");
        return htmlBuilder.toString();
    }

//    public static String generateRandomVerificationCode(int length) {
//        Random random = new Random();
//        StringBuilder sb = new StringBuilder(length);
//        for (int i = 0; i < length; i++) {
//            int randomIndex = random.nextInt(NUMBERS.length());
//            char randomChar = NUMBERS.charAt(randomIndex);
//            sb.append(randomChar);
//        }
//        return sb.toString();
//    }
//    public static boolean sendVerificationCode(String username, String code) {
//        UsersDAO usersDAO = new UsersDAO();
//        Users user = usersDAO.searchUserByUsername(username);
//        String to = user.getEmail();
//        String subject = "[ZSHOP] Verification code";
//
//        Properties props = new Properties();
//        props.put("mail.smtp.host", "smtp.gmail.com");
//        props.put("mail.smtp.socketFactory.port", "465");
//        props.put("mail.smtp.socketFactory.class", "javax.net.ssl.SSLSocketFactory");
//        props.put("mail.smtp.auth", "true");
//        props.put("mail.smtp.prot", "465");
//
//        // Create Authenticator
//        Authenticator auth = new Authenticator() {
//            @Override
//            protected PasswordAuthentication getPasswordAuthentication() {
//                return new PasswordAuthentication(from, password);
//            }
//        };
//
//        Session session = Session.getInstance(props, auth);
//        MimeMessage msg = new MimeMessage(session);
//        try {
//            msg.addHeader("Content-type", "text/HTML; charset = UTF-8");
//            msg.setFrom(new InternetAddress(from));
//            msg.setRecipients(Message.RecipientType.TO, InternetAddress.parse(to, false));
//            msg.setSubject(subject);
//            msg.setSentDate(new Date());
//            msg.setContent("<table style=\"font-family:'Open Sans',sans-serif\" width=\"100%\" border=\"0\">\n"
//                    + "        <tbody>\n"
//                    + "            <tr>\n"
//                    + "                <td style=\"word-break:break-word;padding:28px 33px 25px;font-family:'Open Sans',sans-serif\"\n"
//                    + "                    align=\"left\">\n"
//                    + "                    <div>\n"
//                    + "                        <p style=\"font-size:14px;line-height:200%;\">\n"
//                    + "                            Hi <strong>" + username + "</strong>, here is your verification code: </p>\n"
//                    + "                        <p style=\"font-size:14px;line-height:200%;\">\n"
//                    + "                            <strong>" + code + "</strong></p>\n"
//                    + "                        <p style=\"font-size:14px;line-height:200%;\">\n"
//                    + "                            Any questions please contact: <a href=\"mailto:hieptvhe173252@gmail.com\"\n"
//                    + "                                target=\"_blank\">hieptvhe173252@gmail.com</a> to\n"
//                    + "                            be answered.</p>\n"
//                    + "                    </div>\n"
//                    + "                </td>\n"
//                    + "            </tr>\n"
//                    + "        </tbody>\n"
//                    + "    </table>", "text/html;charset=UTF-8");
//            Transport.send(msg);
//            return true;
//        } catch (MessagingException e) {
//        }
//        return false;
//    }
//
//    public static boolean sendVerificationCodeToEmail(String email, String code) {
//        String subject = "[ZSHOP] Verification code";
//
//        Properties props = new Properties();
//        props.put("mail.smtp.host", "smtp.gmail.com");
//        props.put("mail.smtp.socketFactory.port", "465");
//        props.put("mail.smtp.socketFactory.class", "javax.net.ssl.SSLSocketFactory");
//        props.put("mail.smtp.auth", "true");
//        props.put("mail.smtp.prot", "465");
//
//        // Create Authenticator
//        Authenticator auth = new Authenticator() {
//            @Override
//            protected PasswordAuthentication getPasswordAuthentication() {
//                return new PasswordAuthentication(from, password);
//            }
//        };
//
//        Session session = Session.getInstance(props, auth);
//        MimeMessage msg = new MimeMessage(session);
//        try {
//            msg.addHeader("Content-type", "text/HTML; charset = UTF-8");
//            msg.setFrom(new InternetAddress(from));
//            msg.setRecipients(Message.RecipientType.TO, InternetAddress.parse(email, false));
//            msg.setSubject(subject);
//            msg.setSentDate(new Date());
//            msg.setContent("<table style=\"font-family:'Open Sans',sans-serif\" width=\"100%\" border=\"0\">\n"
//                    + "        <tbody>\n"
//                    + "            <tr>\n"
//                    + "                <td style=\"word-break:break-word;padding:28px 33px 25px;font-family:'Open Sans',sans-serif\"\n"
//                    + "                    align=\"left\">\n"
//                    + "                    <div>\n"
//                    + "                        <p style=\"font-size:14px;line-height:200%;\">\n"
//                    + "                            Hi <strong>" + email + "</strong>, here is your verification code: </p>\n"
//                    + "                        <p style=\"font-size:14px;line-height:200%;\">\n"
//                    + "                            <strong>" + code + "</strong></p>\n"
//                    + "                        <p style=\"font-size:14px;line-height:200%;\">\n"
//                    + "                            Any questions please contact: <a href=\"mailto:hieptvhe173252@gmail.com\"\n"
//                    + "                                target=\"_blank\">hieptvhe173252@gmail.com</a> to\n"
//                    + "                            be answered.</p>\n"
//                    + "                    </div>\n"
//                    + "                </td>\n"
//                    + "            </tr>\n"
//                    + "        </tbody>\n"
//                    + "    </table>", "text/html;charset=UTF-8");
//            Transport.send(msg);
//            return true;
//        } catch (MessagingException e) {
//        }
//        return false;
//    }
//    public static void main(String[] args) {
//        AdminEmailContext context = new AdminEmailContext();
//        StockDAO stockDAO = new StockDAO();
//        String captcha = generateRandomVerificationCode(6);
//        System.out.println(captcha);
//    }
}
