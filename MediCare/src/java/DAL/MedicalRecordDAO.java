/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */

package DAL;

import Models.MedicalRecord;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;

/**
 *
 * @author tubinh
 */
public class MedicalRecordDAO extends DBContext{
    public ArrayList<MedicalRecord> getListMedicalRecordsByAppointmentId (String appointmentId) {
        ArrayList<MedicalRecord> list = new ArrayList<>();
        String sql = "SELECT * FROM MedicalRecords WHERE appointmentId = ?";
        
        try {
            PreparedStatement st = connection.prepareStatement(sql);
            st.setInt(1, Integer.parseInt(appointmentId));
            ResultSet rs = st.executeQuery();
            while (rs.next()) {
                MedicalRecord m = new MedicalRecord(rs.getInt("id") + "" , rs.getString("name"));
                list.add(m);
            }
        } catch (NumberFormatException | SQLException e) {
            System.out.println("getListMedicalRecordsByAppointmentId: " + e);
        }
        return list;
    }
   
    public static void main(String[] args) {
        MedicalRecordDAO md = new MedicalRecordDAO();
        ArrayList <MedicalRecord> list = md.getListMedicalRecordsByAppointmentId("0");
        for (MedicalRecord medicalRecord : list) {
            System.out.println(medicalRecord);
        }
    }
}
