/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package DAL;

import Models.Appointments;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import Models.BillingHistory;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.Iterator;
import java.util.List;

/**
 *
 * @author hoang
 */
public class BillingHistoryDAO extends DBContext {

    public ArrayList<BillingHistory> getListBillingHistory() {
        ArrayList<BillingHistory> list = new ArrayList<>();
        String SQL = "SELECT * FROM [BillingHistory]";

        try ( PreparedStatement ps = connection.prepareStatement(SQL)) {
            ResultSet rs = ps.executeQuery();
            while (rs.next()) {
                BillingHistory bh = new BillingHistory(String.valueOf(rs.getInt(1)),
                        String.valueOf(rs.getInt(2)),
                        String.valueOf(rs.getFloat(3)),
                        String.valueOf(rs.getDate(4)));
                list.add(bh);
            }
            return list;
        } catch (SQLException e) {
            System.out.println("getListBillingHistory: " + e.getMessage());
        }
        return null;
    }

    public List<BillingHistory> getListBillingHistoryByOwnerId(String ownerId) throws ParseException {
        List<BillingHistory> list = new ArrayList<>();
        String SQL = "select bh.id, bh.totalCash, bh.createdAt, a.id AS appointmentId,a.plannedAt,d.displayName, fp.email, fp.name, fp.phone, fp.ownerId\n"
                + "from BillingHistory bh\n"
                + "JOIN Appointments a on bh.appointmentId = a.id\n"
                + "JOIN FamilyProfile fp on a.profileId = fp.profileId\n"
                + "JOIN Doctor d on a.doctorId = d.id\n"
                + "where fp.ownerId = ?";

        try ( PreparedStatement ps = connection.prepareStatement(SQL)) {
            ps.setString(1, ownerId);
            ResultSet rs = ps.executeQuery();
            while (rs.next()) {
                String[] dt = rs.getString("createdAt").split(" ");
                String date = dt[0];
                String[] hour = dt[1].split("\\.");
                String formattedHour = hour[0];

                SimpleDateFormat formatter1 = new SimpleDateFormat("yyyy-MM-dd");
                Date as = formatter1.parse(date);

                SimpleDateFormat formatter2 = new SimpleDateFormat("dd/MM/yyyy");
                String output = formatter2.format(as);
                
                String formattedDate = output + " " + formattedHour;
                
                String[] cash = rs.getString("totalCash").split("\\.");
                BillingHistory bh = new BillingHistory();
                bh.setId(rs.getString("id"));
                bh.setTotalCash(cash[0]);
                bh.setCreatedAt(formattedDate);
                bh.getAppointment().setId(rs.getString("appointmentId"));
                bh.getAppointment().setPlannedAt(rs.getString("plannedAt"));
                bh.getAppointment().getDoctor().setDisplayName(rs.getString("displayName"));
                bh.getAppointment().getFp().setEmail(rs.getString("email"));
                bh.getAppointment().getFp().setName(rs.getString("name"));
                bh.getAppointment().getFp().setPhone(rs.getString("phone"));
                list.add(bh);
            }
            return list;
        } catch (SQLException e) {
            System.out.println("getListBillingHistoryByOwnerId: " + e.getMessage());
        }
        return null;
    }

    public BillingHistory getListBillingHistoryById(String id) {
        BillingHistory bh = new BillingHistory();
        String SQL = "select bh.id, bh.totalCash, bh.createdAt, a.id AS appointmentId,a.plannedAt,d.displayName, fp.email, fp.name, fp.phone, fp.ownerId\n"
                + "from BillingHistory bh\n"
                + "JOIN Appointments a on bh.appointmentId = a.id\n"
                + "JOIN FamilyProfile fp on a.profileId = fp.profileId\n"
                + "JOIN Doctor d on a.doctorId = d.id\n"
                + "where bh.id = ?";

        try ( PreparedStatement ps = connection.prepareStatement(SQL)) {
            ps.setString(1, id);
            ResultSet rs = ps.executeQuery();
            if (rs.next()) {
                String[] cash = rs.getString("totalCash").split("\\.");
                bh = new BillingHistory();
                bh.setId(rs.getString("id"));
                bh.setTotalCash(cash[0]);
                bh.setCreatedAt(rs.getString("createdAt"));
                bh.getAppointment().setId(rs.getString("appointmentId"));
                bh.getAppointment().setPlannedAt(rs.getString("plannedAt"));
                bh.getAppointment().getDoctor().setDisplayName(rs.getString("displayName"));
                bh.getAppointment().getFp().setEmail(rs.getString("email"));
                bh.getAppointment().getFp().setName(rs.getString("name"));
                bh.getAppointment().getFp().setPhone(rs.getString("phone"));
            }
            return bh;
        } catch (SQLException e) {
            System.out.println("getListBillingHistoryById: " + e.getMessage());
        }
        return null;
    }

    public List<BillingHistory> getListBillingHistoryByOwnerIdAndName(String search, String ownerId) throws ParseException {
        List<BillingHistory> list = new ArrayList<>();
        String SQL = "select bh.id, bh.totalCash, bh.createdAt, a.id AS appointmentId,a.plannedAt,d.displayName, fp.email, fp.name, fp.phone, fp.ownerId\n"
                + "from BillingHistory bh\n"
                + "JOIN Appointments a on bh.appointmentId = a.id\n"
                + "JOIN FamilyProfile fp on a.profileId = fp.profileId\n"
                + "JOIN Doctor d on a.doctorId = d.id\n"
                + "where fp.ownerId = ?";

        try ( PreparedStatement ps = connection.prepareStatement(SQL)) {
            ps.setString(1, ownerId);
            ResultSet rs = ps.executeQuery();
            while (rs.next()) {
                String[] dt = rs.getString("createdAt").split(" ");
                String date = dt[0];
                String[] hour = dt[1].split("\\.");
                String formattedHour = hour[0];

                SimpleDateFormat formatter1 = new SimpleDateFormat("yyyy-MM-dd");
                Date as = formatter1.parse(date);

                SimpleDateFormat formatter2 = new SimpleDateFormat("dd/MM/yyyy");
                String output = formatter2.format(as);
                
                String formattedDate = output + " " + formattedHour;
                
                String[] cash = rs.getString("totalCash").split("\\.");
                BillingHistory bh = new BillingHistory();
                bh.setId(rs.getString("id"));
                bh.setTotalCash(cash[0]);
                bh.setCreatedAt(formattedDate);
                bh.getAppointment().setId(rs.getString("appointmentId"));
                bh.getAppointment().setPlannedAt(rs.getString("plannedAt"));
                bh.getAppointment().getDoctor().setDisplayName(rs.getString("displayName"));
                bh.getAppointment().getFp().setEmail(rs.getString("email"));
                bh.getAppointment().getFp().setName(rs.getString("name"));
                bh.getAppointment().getFp().setPhone(rs.getString("phone"));
                list.add(bh);
            }
            Iterator<BillingHistory> iterator = list.iterator();
            while (iterator.hasNext()) {
                BillingHistory bh = iterator.next();
                if (!bh.getAppointment().getFp().getName().toLowerCase().contains(search.toLowerCase())) {
                    iterator.remove();
                }
            }
            return list;
        } catch (SQLException e) {
            System.out.println("getListBillingHistoryByOwnerIdAndName: " + e.getMessage());
        }
        return null;
    }

    public List<BillingHistory> getListBillingHistoryByOwnerIdAndPhone(String search, String ownerId) throws ParseException {
        List<BillingHistory> list = new ArrayList<>();
        String SQL = "select bh.id, bh.totalCash, bh.createdAt, a.id AS appointmentId,a.plannedAt,d.displayName, fp.email, fp.name, fp.phone, fp.ownerId\n"
                + "from BillingHistory bh\n"
                + "JOIN Appointments a on bh.appointmentId = a.id\n"
                + "JOIN FamilyProfile fp on a.profileId = fp.profileId\n"
                + "JOIN Doctor d on a.doctorId = d.id\n"
                + "where fp.ownerId = ?";

        try ( PreparedStatement ps = connection.prepareStatement(SQL)) {
            ps.setString(1, ownerId);
            ResultSet rs = ps.executeQuery();
            while (rs.next()) {
                
                String[] dt = rs.getString("createdAt").split(" ");
                String date = dt[0];
                String[] hour = dt[1].split("\\.");
                String formattedHour = hour[0];

                SimpleDateFormat formatter1 = new SimpleDateFormat("yyyy-MM-dd");
                Date as = formatter1.parse(date);

                SimpleDateFormat formatter2 = new SimpleDateFormat("dd/MM/yyyy");
                String output = formatter2.format(as);
                
                String formattedDate = output + " " + formattedHour;
                
                String[] cash = rs.getString("totalCash").split("\\.");
                BillingHistory bh = new BillingHistory();
                bh.setId(rs.getString("id"));
                bh.setTotalCash(cash[0]);
                bh.setCreatedAt(formattedDate);
                bh.getAppointment().setId(rs.getString("appointmentId"));
                bh.getAppointment().setPlannedAt(rs.getString("plannedAt"));
                bh.getAppointment().getDoctor().setDisplayName(rs.getString("displayName"));
                bh.getAppointment().getFp().setEmail(rs.getString("email"));
                bh.getAppointment().getFp().setName(rs.getString("name"));
                bh.getAppointment().getFp().setPhone(rs.getString("phone"));
                list.add(bh);
            }
            Iterator<BillingHistory> iterator = list.iterator();
            while (iterator.hasNext()) {
                BillingHistory bh = iterator.next();
                if (!bh.getAppointment().getFp().getPhone().toLowerCase().contains(search.toLowerCase())) {
                    iterator.remove();
                }
            }
            return list;
        } catch (SQLException e) {
            System.out.println("getListBillingHistoryByOwnerIdAndName: " + e.getMessage());
        }
        return null;
    }

    public List<BillingHistory> getListBillingHistoryByOwnerIdAndEmail(String search, String ownerId) throws ParseException {
        List<BillingHistory> list = new ArrayList<>();
        String SQL = "select bh.id, bh.totalCash, bh.createdAt, a.id AS appointmentId,a.plannedAt,d.displayName, fp.email, fp.name, fp.phone, fp.ownerId\n"
                + "from BillingHistory bh\n"
                + "JOIN Appointments a on bh.appointmentId = a.id\n"
                + "JOIN FamilyProfile fp on a.profileId = fp.profileId\n"
                + "JOIN Doctor d on a.doctorId = d.id\n"
                + "where fp.ownerId = ?";

        try ( PreparedStatement ps = connection.prepareStatement(SQL)) {
            ps.setString(1, ownerId);

            ResultSet rs = ps.executeQuery();
            while (rs.next()) {
                
                String[] dt = rs.getString("createdAt").split(" ");
                String date = dt[0];
                String[] hour = dt[1].split("\\.");
                String formattedHour = hour[0];

                SimpleDateFormat formatter1 = new SimpleDateFormat("yyyy-MM-dd");
                Date as = formatter1.parse(date);

                SimpleDateFormat formatter2 = new SimpleDateFormat("dd/MM/yyyy");
                String output = formatter2.format(as);
                
                String formattedDate = output + " " + formattedHour;
                
                String[] cash = rs.getString("totalCash").split("\\.");
                BillingHistory bh = new BillingHistory();
                bh.setId(rs.getString("id"));
                bh.setTotalCash(cash[0]);
                bh.setCreatedAt(formattedDate);
                bh.getAppointment().setId(rs.getString("appointmentId"));
                bh.getAppointment().setPlannedAt(rs.getString("plannedAt"));
                bh.getAppointment().getDoctor().setDisplayName(rs.getString("displayName"));
                bh.getAppointment().getFp().setEmail(rs.getString("email"));
                bh.getAppointment().getFp().setName(rs.getString("name"));
                bh.getAppointment().getFp().setPhone(rs.getString("phone"));
                list.add(bh);
            }
            Iterator<BillingHistory> iterator = list.iterator();
            while (iterator.hasNext()) {
                BillingHistory bh = iterator.next();
                if (!bh.getAppointment().getFp().getEmail().toLowerCase().contains(search.toLowerCase())) {
                    iterator.remove();
                }
            }
            return list;
        } catch (SQLException e) {
            System.out.println("getListBillingHistoryByOwnerIdAndName: " + e.getMessage());
        }
        return null;
    }

    public boolean addBill(BillingHistory bh) {
        String SQL = "INSERT INTO [dbo].[BillingHistory]\n"
                + "           ([appointmentId]\n"
                + "           ,[totalCash]\n"
                + "           ,[createdAt])\n"
                + "     VALUES(?,?,?)";

        try ( PreparedStatement ps = connection.prepareStatement(SQL)) {
            ps.setString(1, bh.getAppointmentId());
            ps.setString(2, bh.getTotalCash());
            ps.setString(3, bh.getCreatedAt());
            ps.execute();
            return true;
        } catch (SQLException e) {
            System.out.println("getListBillingHistory: " + e.getMessage());
        }
        return false;
    }

    public int countAllBillingHistory() {
        String SQL = "SELECT COUNT(id) AS count FROM BillingHistory";

        try ( PreparedStatement ps = connection.prepareStatement(SQL)) {
            ResultSet rs = ps.executeQuery();
            if (rs.next()) {
                int count = rs.getInt("count");
                return count;
            }
        } catch (SQLException e) {
            System.out.println("countAllBillingHistory: " + e.getMessage());
        }
        return -1;
    }

    public ArrayList<BillingHistory> getListBillingHistory(int offset, int fetch) {
        ArrayList<BillingHistory> list = new ArrayList<>();
        String SQL = "select bh.id, fp.name, bh.totalCash, bh.createdAt from BillingHistory bh \n"
                + "JOIN Appointments a ON bh.appointmentId = a.id\n"
                + "JOIN FamilyProfile fp ON a.profileId = fp.profileId\n"
                + "ORDER BY bh.id ASC OFFSET ? ROWS FETCH NEXT ? ROWS ONLY";
        
        try ( PreparedStatement ps = connection.prepareStatement(SQL)) {
            ps.setInt(1, offset);
            ps.setInt(2, fetch);
            ResultSet rs = ps.executeQuery();
            while (rs.next()) {
                BillingHistory bh = new BillingHistory();
                bh.setId(rs.getString("id"));
                bh.getAppointment().getFp().setName(rs.getString("name"));
                bh.setTotalCash(rs.getString("totalCash"));
                bh.setCreatedAt(rs.getString("createdAt"));
                list.add(bh);
            }
            return list;
        } catch (SQLException e) {
            System.out.println("getListBillingHistory: " + e.getMessage());
        }
        return null;
    }

    public ArrayList<BillingHistory> getListBillingHistoryByPatientName(int offset, int fetch, String search) {
        ArrayList<BillingHistory> list = new ArrayList<>();
        String SQL = "select bh.id, fp.name, bh.totalCash, bh.createdAt from BillingHistory bh \n"
                + "JOIN Appointments a ON bh.appointmentId = a.id\n"
                + "JOIN FamilyProfile fp ON a.profileId = fp.profileId\n"
                + "ORDER BY bh.id ASC OFFSET ? ROWS FETCH NEXT ? ROWS ONLY";
        
        try ( PreparedStatement ps = connection.prepareStatement(SQL)) {
            ps.setInt(1, offset);
            ps.setInt(2, fetch);
            ResultSet rs = ps.executeQuery();
            while (rs.next()) {
                BillingHistory bh = new BillingHistory();
                bh.setId(rs.getString("id"));
                bh.getAppointment().getFp().setName(rs.getString("name"));
                bh.setTotalCash(rs.getString("totalCash"));
                bh.setCreatedAt(rs.getString("createdAt"));
                list.add(bh);
            }
            Iterator<BillingHistory> iterator = list.iterator();
            while(iterator.hasNext()){
                BillingHistory temp = iterator.next();
                if(!temp.getAppointment().getFp().getName().toLowerCase().contains(search.toLowerCase())){
                    iterator.remove();
                }
            }
            return list;
        } catch (SQLException e) {
            System.out.println("getListBillingHistory: " + e.getMessage());
        }
        return null;
    }
}
