/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package Models;

import java.util.ArrayList;

/**
 *
 * @author DELL
 */
public class AdminSidebarMenu {
    private String name;
    private String link;
    private String icon;
    private ArrayList<AdminSidebarMenu> childrens;
    private String id;

    public AdminSidebarMenu() {
    }

    public AdminSidebarMenu(String name, String link, String icon, ArrayList<AdminSidebarMenu> childrens) {
        this.name = name;
        this.link = link;
        this.icon = icon;
        this.childrens = childrens;
    }

    
    public AdminSidebarMenu(String id, String name, String link, String icon) {
        this.id = id;
        this.name = name;
        this.link = link;
        this.icon = icon;
    }
    public AdminSidebarMenu(String name, String link, String icon) {
        this.name = name;
        this.link = link;
        this.icon = icon;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getLink() {
        return link;
    }

    public void setLink(String link) {
        this.link = link;
    }

    public ArrayList<AdminSidebarMenu> getChildrens() {
        return childrens;
    }

    public void setChildrens(ArrayList<AdminSidebarMenu> childrens) {
        this.childrens = childrens;
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }
    
    

    public String getIcon() {
        return icon;
    }

    public void setIcon(String icon) {
        this.icon = icon;
    }

    @Override
    public String toString() {
        return "AdminSidebarMenu{" + "name=" + name + ", link=" + link + ", icon=" + icon + ", childrens=" + childrens + ", id=" + id + '}';
    }

    
}
