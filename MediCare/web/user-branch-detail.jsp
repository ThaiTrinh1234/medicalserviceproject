<%-- 
    Document   : health
    Created on : Sep 16, 2023, 11:19:22 PM
    Author     : hoang
--%>

<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html>
    <head>
        <!-- basic -->
        <meta charset="utf-8">
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <!-- mobile metas -->
        <meta name="viewport" content="width=device-width, initial-scale=1">
        <meta name="viewport" content="initial-scale=1, maximum-scale=1">
        <!-- site metas -->
        <title>${requestScope.branch.getName()} | MediCare</title>
        <meta name="keywords" content="">
        <meta name="description" content="">
        <meta name="author" content="">
        <jsp:include page="user-head.jsp"/>
    </head>

    <body>
        <%@include file="user-header.jsp" %>
        <!--profile header of a branch start-->
        <div class="branch-profile-header">
            <img src="${pageContext.request.contextPath}/assets/client/images/branch-img.jpg" alt="branch-img" class="branch-image" />
            <p class="branch-name">MediCare - ${requestScope.branch.getName()}</p>
        </div>
        <!--profile header of a branch end-->

        <!--branch information start-->
        <div class="branch-info">
            <div class="intro-header" onclick="toggleInfo()">
                <h3 style='color: #2C6975'>Giới thiệu về chi nhánh ${requestScope.branch.getName()}</h3>
                <span class="toggle-icon">▼</span>
            </div>
            <div class="intro-content"  id="info-content">
                <!-- Information of branch here-->
                <p>${requestScope.branch.description}</p>
            </div>
        </div>

        <!--branch information end-->

        <!-- list doctors in a branch start -->
        <div class="doctors-container">
            <c:forEach items="${requestScope.doctorsOfBranch}" var="doctor">

                <div class="doctor-card">
                    <c:choose>
                        <c:when test="${doctor.getProfilePicture() != null && fn:length(doctor.getProfilePicture()) > 0}">
                            <img src="${doctor.getProfilePicture()}" style="border-radius: 50%; object-fit: cover; width: 100px; height: 100px" alt="alt"/>
                        </c:when>
                        <c:otherwise>
                            <img style="border-radius: 50%; object-fit: cover; width: 100px; height: 100px" src="https://www.svgrepo.com/show/497407/profile-circle.svg" alt="client-img"/> 
                        </c:otherwise>
                    </c:choose>
                    <div class="doctor-info">
                        <h2 class="doctor-name" style='color: #2C6975'>${doctor.getDisplayName()}</h2>
                        <p class="academic-degree">
                            <c:if test="${doctor.getARName()!=null}">
                                ${doctor.getARName()}
                                <c:if test="${doctor.getCertificates()!=null}">
                                    , ${doctor.getCertificates()}
                                </c:if>
                            </c:if>
                            <c:if test="${doctor.getARName()==null}">
                                <c:if test="${doctor.getCertificates()!=null}">
                                    ${doctor.getCertificates()}
                                </c:if>
                            </c:if>
                        </p>
                        <p class="department">Khoa: ${doctor.getDepartmentName()}</p>
                        <p class="branch">${doctor.getBranchName()}</p>
                        <div class="button-container">
                            <button class="book-appointment"> <a class="book-appointment-url" href="user-doctor-detail?doctorId=${doctor.getId()}">Xem chi tiết</a></button>
                        </div>
                    </div>
                </div>
            </c:forEach> 
        </div>
        <!-- list doctors in a branch end-->
        <nav aria-label="Pagination">
            <ul class="pagination justify-content-center">
                <c:if test="${currentPage != 1}">
                    <li class="page-item">
                        <a class="page-link" href="${pageContext.request.contextPath}/user-branch-detail?branchId=${branch.getId()}&page=${currentPage - 1}"><<</a>
                    </li>
                </c:if>

                <c:forEach begin="1" end="${pageCount}" var="i">
                    <c:choose>
                        <c:when test="${currentPage eq i}">
                            <li class="page-item active">
                                <a class="page-link" href="#">${i}</a>
                            </li>
                        </c:when>
                        <c:otherwise>
                            <li class="page-item">
                                <a class="page-link" href="${pageContext.request.contextPath}/user-branch-detail?branchId=${branch.getId()}&page=${i}">${i}</a>
                            </li>
                        </c:otherwise>
                    </c:choose>
                </c:forEach>
                <c:if test="${currentPage lt pageCount}">
                    <li class="page-item">
                        <a class="page-link" href="${pageContext.request.contextPath}/user-branch-detail?branchId=${branch.getId()}&page=${currentPage + 1}">>></a>
                    </li>
                </c:if>
            </ul>
        </nav>

        <%@include file="user-footer.jsp" %>
        <button id="back-to-top" title="Back to top">↑</button>

        <script>
            function toggleInfo() {
                var infoContent = document.getElementById("info-content");
                var toggleIcon = document.querySelector(".toggle-icon");


                console.log("Display=" + infoContent.style.display);
                if (infoContent.style.display === "") {
                    console.log("Display=" + "none");
                    infoContent.style.display = "none";
                } else if (infoContent.style.display === "none") {
                    infoContent.style.display = "block";
                } else if (infoContent.style.display === "block") {
                    infoContent.style.display = "none";
                }
//                if (infoContent.style.display === "none" || infoContent.style.display === "") {
//                    infoContent.style.display = "block";
//                    toggleIcon.innerHTML = "▲"; // Thay đổi biểu tượng khi mở
//                } else {
//                    infoContent.style.display = "none";
//                    toggleIcon.innerHTML = "▼"; // Thay đổi biểu tượng khi đóng
//                }
            }

        </script>
        <%@include file="user-script.jsp" %>
    </body>
</html>
