<%-- 
    Document   : user-forgot-password
    Created on : Nov 15, 2023, 12:55:01 AM
    Author     : tubinh
--%>

<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <!-- basic -->
        <meta charset="utf-8" />
        <meta http-equiv="X-UA-Compatible" content="IE=edge" />
        <!-- mobile metas -->
        <meta name="viewport" content="width=device-width, initial-scale=1" />
        <meta name="viewport" content="initial-scale=1, maximum-scale=1" />
        <!-- site metas -->
        <title>Quên mật khẩu | MediCare</title>
        <meta name="keywords" content="" />
        <meta name="description" content="" />
        <meta name="author" content="" />
        <script src="https://code.jquery.com/jquery-3.6.0.min.js"></script>
        <jsp:include page="user-head.jsp"/>
    </head>
    <body>
        <div class="main-wrapper account-wrapper main-wrapper-forgot-password">
            <div class="card text-center" style="">
                <div class="card-header h5 text-white">Quên mật khẩu</div>
                <div class="card-body px-5">
                    <p class="card-text py-2">
                        Vui lòng nhập địa chỉ email đã đăng ký tài khoản của bạn, MediCare sẽ gửi mật khẩu mới cho bạn!
                    </p>
                    <div class="form-outline">
                        <input type="email" id="email" class="form-control my-3" placeholder="Tài khoản email đã đăng ký... "/>
                        <!--<label class="form-label" for="typeEmail">Email input</label>-->
                    </div>
                    <p class="text-danger" id="error"></p>
                    <a href="#" class="btn btn-primary" onclick="submitEmailForgotPassword(this)">Gửi</a>
                    <div class="d-flex justify-content-between mt-4">
                        <p>Nhớ tài khoản?<a style="color: blue" href="user-login"> Đăng nhập</a></p>
                        <!--<a class="" href="#">Register</a>-->
                    </div>
                </div>
            </div>
        </div>

        <script>
            function submitEmailForgotPassword(event) {
                console.log("Event: submit - email - forgot - password");
                console.log(event);
                var email = document.getElementById("email").value;
                console.log("Email: " + email);

                if (email === "") {
                    document.getElementById("error").innerHTML = "Bạn vui lòng nhập địa chỉ email hợp lệ!";
                } else {
                    // Check in DB:
                    $.ajax({
                        url: "/MediCare/user-forgot-password",
                        data: {
                            email: email,
                            action: "check-email-in-DB"
                        },
                        cache: false,
                        type: "POST",
                        success: function (response) {
                            console.log("Response: " + response);
                            console.log("Type of response: " + typeof response);

                            if (response.localeCompare("success")=== 0) {
                                alert("Chúng tôi sẽ gửi mật khẩu mới vào địa chỉ email bạn vừa cung cấp trong giây lát, vui lòng kiểm tra hòm thư tại email của bạn.");
                                $.ajax({
                                    url: "/MediCare/user-forgot-password",
                                    data: {
                                        email: email,
                                        action: "send-new-password"
                                    },
                                    cache: false,
                                    type: "POST",
                                    success: function (response) {
                                        setTimeout(function () {
                                            window.location.href = "user-login";
                                        }, 100);
                                    },
                                    error: function (xhr) {
                                        alert("Đã xảy ra 1 vài lỗi, vui lòng bấm Gửi để nhận mật khẩu mới.");
                                    }
                                });
                            } else {
                                console.log("string not equal");
                                document.getElementById("error").innerHTML = "Hệ thống không tìm thấy địa chỉ email của bạn đã đăng ký tài khoản, vui lòng kiểm tra lại thông tin.";
                            }
                        },
                        error: function (xhr) {

                        }
                    });
                }
            }
        </script>
        <%@include file="user-script.jsp" %>
    </body>
</html>
