<%-- 
    Document   : user-forgot-password
    Created on : Nov 15, 2023, 12:55:01 AM
    Author     : tubinh
--%>

<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <!-- basic -->
        <meta charset="utf-8" />
        <meta http-equiv="X-UA-Compatible" content="IE=edge" />
        <!-- mobile metas -->
        <meta name="viewport" content="width=device-width, initial-scale=1" />
        <meta name="viewport" content="initial-scale=1, maximum-scale=1" />
        <!-- site metas -->
        <title>Đổi mật khẩu | MediCare</title>
        <meta name="keywords" content="" />
        <meta name="description" content="" />
        <meta name="author" content="" />
        <script src="https://code.jquery.com/jquery-3.6.0.min.js"></script>
        <jsp:include page="user-head.jsp"/>
    </head>
    <body>
        <div class="main-wrapper account-wrapper main-wrapper-forgot-password main-wrapper-change-password">
            <!-- form card change password -->
            <div class="card card-outline-secondary">
                <div class="card-header">
                    <h3 class="mb-0 text-center text-white">Đổi mật khẩu</h3>
                </div>
                <div class="card-body">
                    <!--<form class="form" role="form" autocomplete="off">-->
                    <div class="form-group">
                        <label for="inputPasswordOld">Mật khẩu hiện tại</label>
                        <input placeholder="Mật khẩu hiện tại..." type="password" class="form-control" id="inputPasswordOld" required="">
                    </div>
                    <div class="form-group">
                        <label for="inputPasswordNew">Mật khẩu mới</label>
                        <input placeholder="Mật khẩu mới..." type="password" class="form-control" id="inputPasswordNew" required="">
                        <!--<span class="form-text small text-muted">-->
                        <!--The password must be 8-20 characters, and must <em>not</em> contain spaces.-->
                        <!--</span>-->
                    </div>
                    <div class="form-group">
                        <label for="inputPasswordNewVerify">Nhập lại mật khẩu mới</label>
                        <input placeholder="Nhập lại mật khẩu mới..." type="password" class="form-control" id="inputPasswordNewVerify" required="">
                        <!--<span class="form-text small text-muted">-->
                        <!--To confirm, type the new password again.-->
                        <!--</span>-->
                    </div>
                    <p class="text-danger" id="error"></p>

                    <div class="form-group">
                        <button onclick="submitEmailChangePassword(this)" type="submit" class="btn btn-primary">Lưu</button>
                        <a href="user-home"><button class="btn btn-primary">Hủy</button></a>
                    </div>
                    <!--</form>-->
                </div>
            </div>
            <!-- /form card change password -->
        </div>

        <script>
            function submitEmailChangePassword(event) {
                console.log("Event: submit - email - change - password");
                console.log(event);
                var inputPasswordOld = document.getElementById("inputPasswordOld").value;
                console.log("inputPasswordOld " + inputPasswordOld);
                var inputPasswordNew = document.getElementById("inputPasswordNew").value;
                console.log("inputPasswordNew " + inputPasswordNew);
                var inputPasswordNewVerify = document.getElementById("inputPasswordNewVerify").value;
                console.log("inputPasswordNewVerify " + inputPasswordNewVerify);
                if (inputPasswordOld === "" || inputPasswordNew === "" || inputPasswordNewVerify === "") {
                    document.getElementById("error").innerHTML = "Bạn vui lòng nhập đầy đủ thông tin.";
                } else if (inputPasswordNew !== inputPasswordNewVerify) {
                    document.getElementById("error").innerHTML = "Đảm bảo nhập lại mật khẩu giống với mật khẩu mới.";
                } else if (inputPasswordNew.length < 8) {
                    document.getElementById("error").innerHTML = "Vui lòng nhập mật khẩu mới ít nhất 8 kí tự.";
                } else {
                    document.getElementById("error").innerHTML = "";
                    // Check in DB:
                    $.ajax({
                        url: "/MediCare/user-change-password",
                        data: {
                            oldPassword: inputPasswordOld,
                            action: "check-password-in-DB"
                        },
                        cache: false,
                        type: "POST",
                        success: function (response) {
                            console.log("Response: " + response);
                            console.log("Type of response: " + typeof response);
                            if (response.localeCompare("success") === 0) {
//                                alert("Mật khẩu hiện tại điền đúng!");
                                $.ajax({
                                    url: "/MediCare/user-change-password",
                                    data: {
                                        newPassword: inputPasswordNew,
                                        action: "submit-new-password"
                                    },
                                    cache: false,
                                    type: "POST",
                                    success: function (response) {
                                        alert("Bạn đã đổi mật khẩu thành công.");
                                        setTimeout(function () {
                                            window.location.href = "user-login";
                                        }, 100);
                                    },
                                    error: function (xhr) {
                                        console.log(xhr);
                                        alert("Đã xảy ra 1 vài lỗi, vui lòng bấm Gửi để đổi mật khẩu.");
                                    }
                                });
                            } else {
                                console.log("string not equal");
                                document.getElementById("error").innerHTML = "Mật khẩu hiện tại không đúng, vui lòng kiểm tra lại.";
                            }
                        },
                        error: function (xhr) {

                        }
                    });
                }
            }
        </script>
        <%@include file="user-script.jsp" %>
    </body>
</html>
