<%-- 
    Document   : admin-checkout
    Created on : Nov 15, 2023, 8:52:35 AM
    Author     : phuon
--%>

<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html>
    <head>
        <jsp:include page="../admin-general/admin-head.jsp" />
        <link rel="stylesheet" type="text/css" href="${pageContext.request.contextPath}/assets/admin/css/admin-display-table.css">
    </head>
    <body>
        <div class="main-wrapper">
            <%@include file="../admin-general/admin-header.jsp"%>
            <%@include file="../admin-general/admin-sidebar.jsp"%>
            <div class="page-wrapper">
                <div class="content">
                    <div class="row">
                        <div class="col-sm-4 col-3">
                            <h4 class="page-title">Thanh toán</h4>
                        </div>

                    </div>
                    <h4>Tìm kiếm: </h4>
                    <form action="admin-payment" method="post" style="width: 30%">
                        <input type="text" placeholder="Tên bệnh nhân" name="search" style="border-radius: 10px; border: 1px solid #2c6975">
                        <input type="hidden" id="method" name="method" value="search">
                    </form>
                    <div class="row">
                        <div class="col-md-12">
                            <div class="table-responsive">
                                <table class="table table-striped custom-table">
                                    <thead>
                                        <tr>
                                            <c:forEach var="title" items="${TITLE_APPOINTMENTS}">
                                                ${title.toString()}
                                            </c:forEach>
                                            <th class="text-right">Hành động</th>
                                        </tr>
                                    </thead>
                                    <tbody>
                                        <c:forEach var="list" items="${ALL_APPOINTMENT}">
                                            <tr name="display-table-tr">
                                                <td>${list.getId()}</td>
                                                <td>${list.getUser().getName()}</td>
                                                <td>${list.getDoctor().getDisplayName()}</td>
                                                <td>${list.getServiceTag().getNametag()}</td>
                                                <td>${list.getPlannedAt()}</td>
                                                <td>${list.getStatus()}</td>
                                                <!--<td><span class="custom-badge status-green">Active</span></td>-->
                                                <td class="text-right">
                                                    <button style="background-color: green; color: #fff; cursor: pointer;" onclick="openCheckoutForm(${list.getId()})">Thanh toán</button>
                                                </td>
                                            </tr>
                                        </c:forEach>
                                    </tbody>
                                </table>
                            </div>
                        </div>
                    </div>
                </div>
                <!-- pagination section -->
                <nav aria-label="Pagination">
                    <ul class="pagination justify-content-center">
                        <c:if test="${currentPage != 1}">
                            <li class="page-item">
                                <a class="page-link" href="admin-payment?&page=${currentPage - 1}"><<</a>
                            </li>
                        </c:if>

                        <c:forEach begin="1" end="${pageCount}" var="i">
                            <c:choose>
                                <c:when test="${currentPage eq i}">
                                    <li class="page-item active">
                                        <a class="page-link" href="#">${i}</a>
                                    </li>
                                </c:when>
                                <c:otherwise>
                                    <li class="page-item">
                                        <a class="page-link" href="admin-payment?page=${i}">${i}</a>
                                    </li>
                                </c:otherwise>
                            </c:choose>
                        </c:forEach>
                        <c:if test="${currentPage lt pageCount}">
                            <li class="page-item">
                                <a class="page-link" href="admin-payment?page=${currentPage + 1}">>></a>
                            </li>
                        </c:if>
                    </ul>
                </nav>
                <!-- pagination section -->
                <%@include file="../admin-general/admin-notifications-box.jsp"%>
            </div>
        </div>
        <div id="amountModal" class="modal">
            <!-- Modal content -->
            <div class="modal-content">
                <span class="close" onclick="closeModal()">&times;</span>
                <h2>Nhập số tiền</h2>
                <form id="amountForm" action="admin-payment" method="post">
                    <label for="amount">Tiền:</label>
                    <input type="number" id="amount" name="amount" placeholder="Nhập số tiền cần thanh toán" required min="0">
                    <input type="hidden" id="id" name="id" value="">
                    <input type="hidden" id="method" name="method" value="checkout">
                    <button type="submit" class="submit-btn">Thanh toán</button>
                </form>
            </div>
        </div>           
        <div class="sidebar-overlay" data-reff=""></div>
        <script src="${pageContext.request.contextPath}/assets/admin/js/jquery-3.2.1.min.js"></script>
        <script src="${pageContext.request.contextPath}/assets/admin/js/popper.min.js"></script>
        <script src="${pageContext.request.contextPath}/assets/admin/js/bootstrap.min.js"></script>
        <script src="${pageContext.request.contextPath}/assets/admin/js/jquery.slimscroll.js"></script>
        <script src="${pageContext.request.contextPath}/assets/admin/js/select2.min.js"></script>
        <script src="${pageContext.request.contextPath}/assets/admin/js/app.js"></script>
        <script>
                    // Get the modal
                    var modal = document.getElementById('amountModal');

                    // Get the button that opens the modal
                    var btn = document.querySelector('button');

                    // Get the <span> element that closes the modal
                    var span = document.getElementsByClassName('close')[0];

                    // When the user clicks the button, open the modal
                    btn.onclick = function () {
                        modal.style.display = 'block';
                    };

                    // When the user clicks on <span> (x), close the modal
                    span.onclick = function () {
                        modal.style.display = 'none';
                    };

                    // When the user clicks anywhere outside of the modal, close it
                    window.onclick = function (event) {
                        if (event.target === modal) {
                            modal.style.display = 'none';
                        }
                    };

                    // Function to open the modal
                    function openCheckoutForm(id) {
                        document.getElementById("id").value = id;
                        modal.style.display = 'block';
                    }

                    // Function to close the modal
                    function closeModal() {
                        modal.style.display = 'none';
                    }

                    // Function to handle form submission
                    function submitAmount() {
                        // Get the entered amount
                        var amount = document.getElementById('amount').value;

                        // Perform any further processing with the entered amount
                        alert('Amount submitted: ' + amount);

                        // Close the modal
                        closeModal();
                    }
        </script>
    </body>
</body>
</html>

