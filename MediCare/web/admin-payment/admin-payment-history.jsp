<%-- 
    Document   : patients
    Created on : Sep 20, 2023, 12:17:41 AM
    Author     : Asus
--%>

<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html>
    <head>
        <jsp:include page="../admin-general/admin-head.jsp" />
        <link rel="stylesheet" type="text/css" href="${pageContext.request.contextPath}/assets/admin/css/admin-display-table.css">
    </head>
    <body>
        <div class="main-wrapper">
            <%@include file="../admin-general/admin-header.jsp"%>
            <%@include file="../admin-general/admin-sidebar.jsp"%>
            <div class="page-wrapper">
                <div class="content">
                    <div class="row">
                        <div class="col-sm-4 col-3">
                            <h4 class="page-title">Lịch sử thanh toán</h4>
                        </div>
                    </div>
                    <h4>Tìm kiếm: </h4>
                    <form action="admin-payment-history" method="post" style="width: 30%">
                        <input type="text" placeholder="Tên bệnh nhân" name="search" style="border-radius: 10px; border: 1px solid #2c6975">
                        <input type="hidden" id="method" name="method" value="search">
                    </form>
                    <div class="row">
                        <div class="col-md-12">
                            <div class="table-responsive">
                                <table class="table table-striped custom-table">
                                    <thead>
                                        <tr>
                                            <th>ID</th>
                                            <th>Tên bệnh nhân</th>
                                            <th>Tổng tiền</th>
                                            <th>Ngày thanh toán</th>
                                        </tr>
                                    </thead>
                                    <tbody>
                                        <c:forEach var="list" items="${requestScope.bhList}">
                                            <tr name="display-table-tr">
                                                <td>${list.getId()}</td>
                                                <td>${list.getAppointment().getFp().getName()}</td>
                                                <td>${list.getTotalCash()}</td>
                                                <td>${list.getCreatedAt()}</td>
                                                <!--<td><span class="custom-badge status-green">Active</span></td>-->
                                            </tr>
                                        </c:forEach>
                                    </tbody>
                                </table>
                            </div>
                        </div>
                    </div>
                </div>
                <!-- pagination section -->
                <nav aria-label="Pagination">
                    <ul class="pagination justify-content-center">
                        <c:if test="${currentPage != 1}">
                            <li class="page-item">
                                <a class="page-link" href="admin-payment-history&page=${currentPage - 1}"><<</a>
                            </li>
                        </c:if>

                        <c:forEach begin="1" end="${pageCount}" var="i">
                            <c:choose>
                                <c:when test="${currentPage eq i}">
                                    <li class="page-item active">
                                        <a class="page-link" href="#">${i}</a>
                                    </li>
                                </c:when>
                                <c:otherwise>
                                    <li class="page-item">
                                        <a class="page-link" href="admin-payment-history?page=${i}">${i}</a>
                                    </li>
                                </c:otherwise>
                            </c:choose>
                        </c:forEach>
                        <c:if test="${currentPage lt pageCount}">
                            <li class="page-item">
                                <a class="page-link" href="admin-payment-history?page=${currentPage + 1}">>></a>
                            </li>
                        </c:if>
                    </ul>
                </nav>
                <!-- pagination section -->
                <%@include file="../admin-general/admin-notifications-box.jsp"%>
            </div>
        </div>

        <div class="sidebar-overlay" data-reff=""></div>
        <script src="${pageContext.request.contextPath}/assets/admin/js/jquery-3.2.1.min.js"></script>
        <script src="${pageContext.request.contextPath}/assets/admin/js/popper.min.js"></script>
        <script src="${pageContext.request.contextPath}/assets/admin/js/bootstrap.min.js"></script>
        <script src="${pageContext.request.contextPath}/assets/admin/js/jquery.slimscroll.js"></script>
        <script src="${pageContext.request.contextPath}/assets/admin/js/select2.min.js"></script>
        <script src="${pageContext.request.contextPath}/assets/admin/js/app.js"></script>
        <script>
            $(function () {
                $('#datetimepicker3').datetimepicker({
                    format: 'LT'
                });
                $('#datetimepicker4').datetimepicker({
                    format: 'LT'
                });
            });
        </script>
        <script>
            function confirmDelete(delUrl) {
                if (confirm("Bạn có chắc chắn xóa không ?")) {
                    document.location = delUrl;
                }
            }
        </script>
    </body>
</body>
</html>
