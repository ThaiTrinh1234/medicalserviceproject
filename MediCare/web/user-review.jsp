<%-- 
    Document   : user-review
    Created on : Nov 16, 2023, 8:14:18 AM
    Author     : hoang
--%>

<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html>
    <head>
        <jsp:include page="user-head.jsp"/>
        <title>Viết đánh giá | MediCare</title>
    </head>
    <body>
        <jsp:include page="user-header.jsp"/>
    </div>
    <div class="container">
        <div class="row">
            <div class="col-md-3">
                <label for="doctor-name">
                    <p>Bác sĩ phụ trách:</p>
                </label><br>
                <label for="service-name">
                    <p>Dịch vụ cung cấp:</p>
                </label><br>
                <label for="planned-at">
                    <p>Được đặt lúc:</p>
                </label><br>
                <label for="symptoms">
                    <p>Triệu chứng:</p>
                </label><br>
                <label for="rating">
                    <p>Đánh giá:</p>
                </label><br>
                <label for="comment">
                    <p>Nhận xét:</p>
                </label><br>
            </div>
            <div class="col-md-9">
                <p>${a.doctor.displayName}</p>
                <p>${a.serviceTag.nametag}</p>
                <p>${a.plannedAt}</p>
                <p>${a.symptoms}</p>
                <!-- star rating here -->
                <textarea id="comment" name="comment" rows="5" cols="40" style="resize: none">${content}</textarea>
            </div>
        </div>
    </div>
    <jsp:include page="user-footer.jsp"/>
    <button id="back-to-top" title="Back to top">↑</button>
    <jsp:include page="user-script.jsp"/>
</body>
</html>
