##### !IMPORTANT

#### Depencency
- This project have the following depencency, please download the exact version if the project doesn't work:
+ apache-tomcat-10.0.27 - jre1.8.0_361
+ jdk1.8.8_111
+ libs/activation.jar
+ libs/jakarta.servlet.jsp.jstl-2.0.0.jar
+ libs/jakarta.servlet.jsp.jstl-api-2.0.0.jar
+ libs/logback-classic-1.3.11-javadoc.jar
+ libs/logback-classic-1.3.11-sources.jar
+ libs/logback-classic-1.3.11.jar
+ libs/logback-core-1.3.11.jar
+ libs/lucene-core-8.11.2.jar
+ libs/mail.jar
+ libs/slf4j-api-2.0.7.jar
+ libs/slf4j.jar
+ libs/sqljdbc42.jar
+ libs/poi/SparseBitSet-1.3.jar
+ libs/poi/commons-codec-1.16.0.jar
+ libs/poi/commons-collections4-4.4.jar
+ libs/poi/commons-compress-1.24.0.jar
+ libs/poi/commons-io-2.13.0.jar
+ libs/poi/commons-math3-3.6.1.jar
+ libs/poi/curvesapi-1.08.jar
+ libs/poi/log4j-api-2.20.0.jar
+ libs/poi/poi-5.2.4.jar
+ libs/poi/poi-examples-5.2.4.jar
+ libs/poi/poi-ooxml-5.2.4.jar
+ libs/poi/poi-ooxml-lite-5.2.4.jar
+ libs/poi/poi-scratchpad-5.2.4.jar
+ libs/poi/xmlbeans-5.1.1.jar
+ libs/iTextlib/barcodes-8.0.2.jar
+ libs/iTextlib/bouncy-castle-adapter-8.0.2.jar
+ libs/iTextlib/bouncy-castle-connector-8.0.2.jar
+ libs/iTextlib/bouncy-castle-fips-adapter-8.0.2.jar
+ libs/iTextlib/commons-8.0.2.jar
+ libs/iTextlib/font-asian-8.0.2.jar
+ libs/iTextlib/forms-8.0.2.jar
+ libs/iTextlib/html2pdf-5.0.2.jar
+ libs/iTextlib/hyph-8.0.2.jar
+ libs/iTextlib/io-8.0.2.jar
+ libs/iTextlib/kernel-8.0.2.jar
+ libs/iTextlib/layout-8.0.2.jar
+ libs/iTextlib/pdfa-8.0.2.jar
+ libs/iTextlib/pdftest-8.0.2.jar
+ libs/iTextlib/sign-8.0.2.jar
+ libs/iTextlib/styled-xml-parser-8.0.2.jar
+ libs/iTextlib/svg-8.0.2.jar
+ libs/AccessGoogle-lib/commons-logging-1.2.jar
+ libs/AccessGoogle-lib/fluent-hc-4.5.5.jar
+ libs/AccessGoogle-lib/gson-2.8.2.jar
+ libs/AccessGoogle-lib/httpclient-4.5.5.jar
+ libs/AccessGoogle-lib/httpcore-4.4.9.jar

# CHANGE LOG

##### This document started writing from 4-10-2023 to take note about changes made in the project branch.

### CHANGELOG 10-11 (v0.9.1)
+ This project changes too many times that I didn't record it.
+ Fix some bugs about the filters from Review and Appointment History page, to make sure that they works perfectly.
+ Fix the header display for associated role, add a filter to slow down the page.

### CHANGELOG 09-10

##### Code changes:
+ Added News viewing screen (view content, view sidebar - in a basic view)
+ Added script for new attributes from `Doctor` and `News`, also `NewsCategory`

##### Database changes:
+ Added `isDelete`, `gender`, `birthDate` for `Doctor`, `symptom` for `Appointment`. ANY OTHER CHANGES ARE NOT ACCEPTED.
+ Updated data for `News` and `NewsCategory`

### CHANGELOG 07-10

##### Database Structure:
+ Added `ownerId` to FamilyProfile
+ Also added a trigger to automatically create a new profile when add a new User object.
+ Alter some attributes' type from `time(7)` to `DATETIME`

##### Code changelog
+ Merged with Jack's code.
+ Minify CSS file. (more minify is recommended)
+ Deleted unnecessary JSP files which remain from conversion to JSP

### CHANGELOG 04-10

##### Database Structure:
+ Added TopLevelMenu and SubLevelMenu tables for satisfying teacher requirements. Now it's applying into header (navbar) and footer (everything except 'Bác sĩ tiêu biểu'). Those information not change too frequently.
+ Removed Contacts, UsefulLinks, SocialNetworks, NavigationItem and more. (Replaced with tables above).

##### Code changelog
- Added associated classes with added tables (Object, DAO).
- Updated `user-header.jsp` and `user-footer.jsp` for displaying using TopLevelMenu and SubLevelMenu tables/classes.
- Added missing `UserServiceDetailServlet.java`
- Added pagination for `user-search-result.jsp` and `user-list-all-doctor.jsp`